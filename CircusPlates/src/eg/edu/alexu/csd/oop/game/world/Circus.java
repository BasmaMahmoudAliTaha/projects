package eg.edu.alexu.csd.oop.game.world;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import eg.edu.alexu.csd.oop.game.DynamicLinkageShapes;
import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.LevelsStrategy;
import eg.edu.alexu.csd.oop.game.Log4j;
import eg.edu.alexu.csd.oop.game.World;
import eg.edu.alexu.csd.oop.game.object.ConstantObjects;
import eg.edu.alexu.csd.oop.game.object.DetermineMovingState;
import eg.edu.alexu.csd.oop.game.object.ImageObject;
import eg.edu.alexu.csd.oop.game.object.MovingObjects;

public class Circus implements World {
	private int maxTime;
	private long startTime = System.currentTimeMillis();
	private final int width, height;
	private int prevScore = 0, winningScore;
	private List<GameObject> constant = new ArrayList<GameObject>();
	private List<GameObject> moving = new ArrayList<GameObject>();
	private List<GameObject> control = new ArrayList<GameObject>();
	private DetermineMovingState determineMovingState;
	private List<String> supportedShapes;
	private long time;
	private int rateOfCreaatingObjects , controlSpeed, gameSpeed, shapesLevel, numberOfShapes,
			numOfControllers , handWidth , verticalSpaceBtwObjects;
	private String clown , background , levelType;
	private final int MAXNUMOFOBJS;
	// private ImageObject infoPanel;
	/*
	 * We need to use this object in a method to set level strategy's variables
	 * That's when we decide what we gonna do in each one And what are the vars.
	 */
	public LevelsStrategy levelHardness;

	public Circus(int screenWidth, int screenHeight, int maxNumOfObjs) {
		width = screenWidth;
		height = screenHeight;
		MAXNUMOFOBJS = maxNumOfObjs;
		//constant.add(new ImageObject(0, 0, "/background.jpg"));
		// infoPanel = new ImageObject(20, 500,null);
	}

	// If we want to dynamically set the levels at run time.
	public void setGameLevel(LevelsStrategy newLevelHardness) {
		levelHardness = newLevelHardness;
	}

	public void setLevelProperty() {
		LinkedHashMap<String, String> levelsProperties = levelHardness
				.strategy();
		//int counter = 1;
		//for (Map.Entry<String, Integer> entry : levelsProperties.entrySet()) {
			//switch (counter) {
			//case (1):
				gameSpeed = Integer.parseInt(levelsProperties.get("gameSpeed"));
				//break;
			//case (2):
				controlSpeed = Integer.parseInt(levelsProperties.get("controlSpeed"));
				//break;
			//case (3):
				System.out.println(levelsProperties.get("numOfShapes"));
				numberOfShapes = Integer.parseInt(levelsProperties.get("numOfShapes"));
				//break;
			//case (4):
				shapesLevel = Integer.parseInt(levelsProperties.get("shapesLevel"));
				//break;
			//case (5):
				numOfControllers = Integer.parseInt(levelsProperties.get("numOfControllers"));
				//break;
			//case (6):
				maxTime = Integer.parseInt(levelsProperties.get("maxTime"));
				//break;
			//case (7):
				winningScore = Integer.parseInt(levelsProperties.get("winningScore"));
				clown = levelsProperties.get("HeroName");
				background = levelsProperties.get("background");
				handWidth = Integer.parseInt(levelsProperties.get("handWidth"));
				levelType = levelsProperties.get("levelType");
				verticalSpaceBtwObjects = Integer.parseInt(levelsProperties.get("verticalSpace"));
				//break;
			//}
			//counter++;
		//}
		Log4j.getInstance().getLogger().info("gameSpeed-" + gameSpeed);
		Log4j.getInstance().getLogger().info("controlSpeed-" + controlSpeed);
		Log4j.getInstance().getLogger()
		.info("numberOfShapes-" + numberOfShapes);
		Log4j.getInstance().getLogger().info("shapesLevel- " + shapesLevel);
		Log4j.getInstance().getLogger()
		.info("numOfControllers-" + numOfControllers);
	}

	public void intializeField(int spaceBetnClowns, int firstLineWidth,
			int verticalSpaceBetnConsts ) {
		try {
			constant.add(new ImageObject(0, 0, "/"+background+".jpg"));
			supportedShapes = DynamicLinkageShapes.getInstance().addPlugin();
			for (String s : supportedShapes)
				Log4j.getInstance().getLogger().debug(s + " ****** ");
			List<String> shapes = new ArrayList<String>();
			if (levelType.equals("clown")) 
				shapes.add("Hat");
			else if(levelType.equals("smurf")){
				shapes.add("SmurfGirl");
				shapes.add("Smurf");
			}else if(levelType.equals("csed")){
				shapes.add("Abd");
				shapes.add("Ahmed");
				shapes.add("Sherouk");
				shapes.add("Basma");
			}
			supportedShapes = shapes;
			Log4j.getInstance().getLogger().debug("True");
			
			MovingObjects.setSupportedShapes(supportedShapes);
		} catch (ClassNotFoundException | IllegalAccessException
				| InstantiationException | IOException e) {
			Log4j.getInstance().getLogger().fatal("ERROR LOADING SHAPES");
		}
		int dummySpace = 200;
		int dummyBeginSpace = numOfControllers - 1;
		for (int i = 0; i < numOfControllers; i++) {
			ImageObject clown = new ImageObject((width - spaceBetnClowns),
					height - 150, "/"+this.clown+".png");
			clown.setEnd(width - dummySpace);
			clown.setStart(dummyBeginSpace * clown.getWidth());
			control.add(clown);
			spaceBetnClowns += 300;
			dummySpace += clown.getWidth();
			dummyBeginSpace--;
		}
		verticalSpaceBetnConsts = verticalSpaceBtwObjects ;
		int dummyVeticalSpaceBtwObjects = verticalSpaceBetnConsts;
		for (int i = 1; i <= shapesLevel / 2; i++) {

			Log4j.getInstance().getLogger()
			.info("vertical Space" + verticalSpaceBetnConsts);
			Log4j.getInstance().getLogger()
			.info("posX Right" + firstLineWidth);
			constant.add(new ConstantObjects(0, verticalSpaceBetnConsts,
					Color.orange, firstLineWidth, 5));
			constant.add(new ConstantObjects(width - firstLineWidth,
					verticalSpaceBetnConsts, Color.orange, firstLineWidth, 5));
			verticalSpaceBetnConsts += dummyVeticalSpaceBtwObjects;
			firstLineWidth /= 2;
		}
		Log4j.getInstance().getLogger()
		.debug("Moving Objects Size" + " " + moving.size());
		determineMovingState = DetermineMovingState.getInstance(height, moving,
				control, handWidth);
		Pool.getInstance().intializePool(MAXNUMOFOBJS, determineMovingState);
	}

	@Override
	public boolean refresh() {
		boolean timeout = System.currentTimeMillis() - startTime > maxTime;
		time =  Math.max(0,(maxTime - (System.currentTimeMillis() - startTime)) / 1000);
		int levels = this.shapesLevel / 2;
		int posY = 20;
		int i = 0;
		
		for (int j = 0; j < moving.size(); j++) {
			MovingObjects movingObj = (MovingObjects) moving.get(j);
			determineMovingState.setObjectState(movingObj);
			GameObject g = movingObj.getMoveState().move(movingObj,
					movingObj.isObjDirec());
			movingObj.getMoveState().intersect(movingObj,
					movingObj.isObjDirec());
			if (determineMovingState.isCatched(movingObj,
					movingObj.isObjDirec()))
				control = determineMovingState.getControllor();
			if (g == null)
				j--;
		}
		if(LifeCounter.getInstance().getLifes()==0){
			timeout = true;
			time = 0;
		}
			
		if (rateOfCreaatingObjects == 0)
			while (levels-- > 0 && !timeout && winningScore > 0) {
				GameObject constObject = constant.get(i + 1);
				MovingObjects gameObjectRight = (MovingObjects) Pool
						.getInstance().requestObject();
				gameObjectRight.setObjDirec(true);
				MovingObjects gameObjectLeft = (MovingObjects) Pool
						.getInstance().requestObject();
				gameObjectRight.setLevelPlace(width - constObject.getWidth());
				gameObjectLeft.setLevelPlace(constObject.getWidth());
				gameObjectLeft.setObjDirec(false);
				gameObjectRight.setX(width - 40);
				gameObjectLeft.setX(0);
				gameObjectRight.setY(posY);
				gameObjectLeft.setY(posY);
				moving.add(gameObjectRight);
				moving.add(gameObjectLeft);
				posY += verticalSpaceBtwObjects;
				i += 2;
			}


		rateOfCreaatingObjects++;
		if (rateOfCreaatingObjects == 100)
			rateOfCreaatingObjects = 0;
		if (prevScore < Score.getInstance().getScore()) {
			prevScore = Score.getInstance().getScore();
			winningScore--;
			Log4j.getInstance().getLogger().debug("prevScore is " + prevScore);
		}
		
		 if(winningScore <= 0){
			 timeout = true;
		 }
		return !timeout;
	}

	@Override
	public int getSpeed() {
		return gameSpeed;

	}

	@Override
	public int getControlSpeed() {
		return controlSpeed;
	}

	@Override
	public List<GameObject> getConstantObjects() {
		return constant;
	}

	@Override
	public List<GameObject> getMovableObjects() {
		return moving;
	}

	@Override
	public List<GameObject> getControlableObjects() {
		return control;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public String getStatus() {
		return ("Score "
				+ Score.getInstance().getScore()
				+ " \t Time= "+time+"\t "
						+ "Num of lives  " + LifeCounter.getInstance().getLifes()); // update
		// status
	}
}
