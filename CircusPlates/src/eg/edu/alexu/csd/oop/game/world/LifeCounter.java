package eg.edu.alexu.csd.oop.game.world;

import java.util.Observable;
import java.util.Observer;

import eg.edu.alexu.csd.oop.game.Log4j;

public class LifeCounter implements Observer{

	private int numOfLives;
	
	private LifeCounter(){
		numOfLives = 3;
	}
	
	private static LifeCounter lifeCounter = new LifeCounter();
	
	public static LifeCounter getInstance(){
		return lifeCounter;
	}
	
	public void reset(){
		lifeCounter = new LifeCounter();
	}
	
	public int getLifes(){
		return numOfLives;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if(((int)arg1) == 2){
			Log4j.getInstance().getLogger().debug(numOfLives);
			numOfLives--;
			Log4j.getInstance().getLogger().debug(numOfLives);
		}
	}
}
