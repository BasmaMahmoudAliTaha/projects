package eg.edu.alexu.csd.oop.game;

import java.util.LinkedHashMap;

public interface LevelsStrategy {
	/*Returns Hash map with key of name of the variable
	 * And integer value of this var.
	 * Map contents are set for each level
	 * set of keys {controlSpeed, gameSpeed, shapesLevels, numberOfShapes, numOfControllers}
	 */
	/*
	 * Order of map
	 *			 case(1):
					gameSpeed
				case(2):
					controlSpeed
				case(3):
					numberOfShapes 
				case(4):
					shapesLevel 
				case(5):
					numOfControllers 
				case(6):
					MAX_TIME
	 * 
	 * */
	public LinkedHashMap<String, String> strategy();
}

class LevelOneStrategy implements LevelsStrategy{

	@Override
	public LinkedHashMap<String, String> strategy() {
		LinkedHashMap<String, String> levelProps = new LinkedHashMap<String, String>();
		levelProps.put("gameSpeed", "10");
		levelProps.put("controlSpeed", "10");
		levelProps.put("numOfShapes", "2");
		levelProps.put("shapesLevel", "4");
		levelProps.put("numOfControllers", "2");
		int time = 5 * 60 * 1000;
		levelProps.put("maxTime", String.valueOf(time));
		levelProps.put("winningScore", "10");
		levelProps.put("HeroName", "clown");
		levelProps.put("handWidth", "45");
		levelProps.put("background", "background");
		levelProps.put("levelType", "clown");
		levelProps.put("verticalSpace", "50");

		return levelProps;
	}
	
}
class LevelTwoStrategy implements LevelsStrategy{

	@Override
	public LinkedHashMap<String, String> strategy() {
		LinkedHashMap<String, String> levelProps = new LinkedHashMap<String, String>();
		levelProps.put("gameSpeed", "5");
		levelProps.put("controlSpeed", "10");
		levelProps.put("numOfShapes", "2");
		levelProps.put("shapesLevel", "6");
		levelProps.put("numOfControllers", "3");
		int time = 3* 60 * 1000;
		levelProps.put("maxTime",  String.valueOf(time));
		levelProps.put("winningScore", "20");
		levelProps.put("HeroName", "smurfPapa");
		levelProps.put("handWidth", "35");
		levelProps.put("background", "smurfVillage");
		levelProps.put("levelType", "smurf");
		levelProps.put("verticalSpace", "60");
		return levelProps;
	}
	
}

class LevelThreeStrategy implements LevelsStrategy{

	@Override
	public LinkedHashMap<String, String> strategy() {
		LinkedHashMap<String, String> levelProps = new LinkedHashMap<String, String>();
		levelProps.put("gameSpeed", "-10");
		levelProps.put("controlSpeed","10");
		levelProps.put("numOfShapes", "4");
		levelProps.put("shapesLevel", "8");
		levelProps.put("numOfControllers", "4");
		int time = 2* 60 * 1000;
		levelProps.put("maxTime",  String.valueOf(time));
		levelProps.put("winningScore", "30");
		levelProps.put("HeroName", "eng");
		levelProps.put("background", "csed");
		levelProps.put("handWidth", "35");
		levelProps.put("levelType", "csed");
		levelProps.put("verticalSpace", "45");
		return levelProps;		
	}
	
}