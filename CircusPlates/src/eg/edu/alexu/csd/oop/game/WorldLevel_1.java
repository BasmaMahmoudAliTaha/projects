package eg.edu.alexu.csd.oop.game;

import eg.edu.alexu.csd.oop.game.world.Circus;
/*
 *In level one there is two clowns and  the game speed is 5, and control speed is 10 
 *This values of speed are the default
 *The number of constants are the default which are 2
 */
public class WorldLevel_1 extends Circus{

	public WorldLevel_1(int screenWidth, int screenHeight, int maxNumOfObjs) {
		
		super(screenWidth, screenHeight, maxNumOfObjs);
		levelHardness = new LevelOneStrategy();
	}
}