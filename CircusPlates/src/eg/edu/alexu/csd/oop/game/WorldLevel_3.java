package eg.edu.alexu.csd.oop.game;

import eg.edu.alexu.csd.oop.game.world.Circus;

public class WorldLevel_3 extends Circus{

	public WorldLevel_3(int screenWidth, int screenHeight, int maxNumOfObjs) {
		
		super(screenWidth, screenHeight, maxNumOfObjs);
		levelHardness = new LevelThreeStrategy();
	}

}
