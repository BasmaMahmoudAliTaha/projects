package eg.edu.alexu.csd.oop.game;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * File data order:
 * width
 * height
 * max num of objs
 * const distance between clowns
 * First Line length (initially it is set then we change it dynamically in the code.)
 * Constant vertical distance between lines(that holds the plates)
 * */
public class ReadConfig {
	private final int WIDTH,HEIGHT,NUMOFOBJS,DISTANCEBETWEENCLOWNS,FIRSTLINELENGTH,VERTICALDISTANCEBETNLINES;
	
	
//	private final int LEVELS;
	
	public ReadConfig() {
		File config = new File(System.getProperty("user.dir") +"/" +"config.txt");
		List<Integer> arr = null ;
		try {
			BufferedReader br = new BufferedReader(new FileReader(config));
			String line;
			arr = new ArrayList<Integer>();
			while ((line = br.readLine()) != null) {
				arr.add(Integer.parseInt(line));
			}

			br.close();
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			Log4j.getInstance().getLogger()
					.fatal("Cofiguration File not found");
		}
		WIDTH = arr.get(0);
		HEIGHT = arr.get(1);
		NUMOFOBJS = arr.get(2);
		DISTANCEBETWEENCLOWNS = arr.get(3);
		FIRSTLINELENGTH =  arr.get(4);
		VERTICALDISTANCEBETNLINES=  arr.get(5);
	}

	public int getWidth() {
		return WIDTH;
	}

	public int getHeight() {
		return HEIGHT;
	}

	public int getNumOfObjs() {
		return NUMOFOBJS;
	}

	public int getDISTANCEBETWEENCLOWNS() {
		return DISTANCEBETWEENCLOWNS;
	}

	public int getFIRSTLINELENGTH() {
		return FIRSTLINELENGTH;
	}

	public int getVERTICALDISTANCEBETNLINES() {
		return VERTICALDISTANCEBETNLINES;
	}
	
//	public int getLevels() {
//		return LEVELS;
//	}
	
}
