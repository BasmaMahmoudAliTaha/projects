package eg.edu.alexu.csd.oop.game;

import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import eg.edu.alexu.csd.oop.game.GameEngine.GameController;
import eg.edu.alexu.csd.oop.game.world.Circus;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;

public class StartGame {

	private JFrame frame;
	private  Circus circus;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StartGame window = new StartGame();
					window.initialize();
					window.frame.setVisible(true);
					window.frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					Log4j.getInstance().getLogger().fatal("Error!\nCouldn't load the game!");
				}
			}
		});
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		int screenWid, screenHei;
		frame = new JFrame();
		
		frame.setContentPane(new JLabel(new ImageIcon(this.getClass().getResource("/background.jpg"))));
		
		ReadConfig readConfig = new ReadConfig();
		screenHei = readConfig.getHeight();
		screenWid = readConfig.getWidth();
		
		frame.setBounds(100, 100, readConfig.getWidth(), readConfig.getHeight());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		/****************************************/
		JMenuBar  menuBar = new JMenuBar();
		JMenu menu = new JMenu("File");
//		JMenuItem newMenuItem = new JMenuItem("New");
		JMenuItem pauseMenuItem = new JMenuItem("Pause");
		JMenuItem resumeMenuItem = new JMenuItem("Resume");
		menu.add(pauseMenuItem);
		menu.addSeparator();
		menu.add(resumeMenuItem);
		menuBar.add(menu);
		/****************************************/
		
		JLabel logo = new JLabel(new ImageIcon(this.getClass().getResource("/circuslogo.png")));
		logo.setBounds(screenWid/2-200, screenHei/2, 400, 400);
		frame.getContentPane().add(logo);
		
		JLabel gameTitle = new JLabel();
		gameTitle.setText("Circus Of Plates");
		gameTitle.setFont(new Font("DejaVu Serif", Font.BOLD, 50));
		gameTitle.setBounds((screenWid/2)-250,screenHei/10, 500, 100);
		gameTitle.setForeground(Color.ORANGE);
		frame.getContentPane().add(gameTitle);
		/****************************************/
		JButton btnLevelOne = new JButton("Easy");
		btnLevelOne.setForeground(new Color(139, 0, 0));
		btnLevelOne.setBackground(new Color(244, 164, 96));
		btnLevelOne.setFont(new Font("WenQuanYi Micro Hei Mono", Font.BOLD, 16));

		
		btnLevelOne.setBounds((screenWid/2)-300/2,screenHei/3, 300, screenHei/15);

		btnLevelOne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				circus = new WorldLevel_1(readConfig.getWidth(), readConfig.getHeight(), readConfig.getNumOfObjs());
				circus.setLevelProperty();
				circus.intializeField(readConfig.getDISTANCEBETWEENCLOWNS(),readConfig.getFIRSTLINELENGTH(),readConfig.getVERTICALDISTANCEBETNLINES());
				
				GameController gameController = GameEngine.start("Circus Of Plates", circus,menuBar);
				
				pauseMenuItem.addActionListener(new ActionListener() {
					@Override public void actionPerformed(ActionEvent e) {
						gameController.pause();
					}
				});
				resumeMenuItem.addActionListener(new ActionListener() {
					@Override public void actionPerformed(ActionEvent e) {
						gameController.resume();
					}
				});
			}
		});
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(btnLevelOne);

		JButton btnLevelTwo = new JButton("Medium");
		btnLevelTwo.setForeground(new Color(139, 0, 0));
		btnLevelTwo.setBackground(new Color(244, 164, 96));
		btnLevelTwo.setFont(new Font("WenQuanYi Micro Hei Mono", Font.BOLD, 16));
		btnLevelTwo.setBounds((screenWid/2)-300/2,(screenHei/3)+10+screenHei/15, 300, screenHei/15);

		btnLevelTwo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				circus = new WorldLevel_2(readConfig.getWidth(), readConfig.getHeight(), readConfig.getNumOfObjs());
				circus.setLevelProperty();
				circus.intializeField(readConfig.getDISTANCEBETWEENCLOWNS(),readConfig.getFIRSTLINELENGTH(),readConfig.getVERTICALDISTANCEBETNLINES());
				GameController gameController =GameEngine.start("Circus Of Plates", circus, menuBar);
				
				pauseMenuItem.addActionListener(new ActionListener() {
					@Override public void actionPerformed(ActionEvent e) {
						gameController.pause();
					}
				});
				resumeMenuItem.addActionListener(new ActionListener() {
					@Override public void actionPerformed(ActionEvent e) {
						gameController.resume();
					}
				});
			}
		});
		frame.getContentPane().add(btnLevelTwo);

		JButton btnLevelThree = new JButton("Hard");
		btnLevelThree.setForeground(new Color(139, 0, 0));
		btnLevelThree.setBackground(new Color(244, 164, 96));
		btnLevelThree.setFont(new Font("WenQuanYi Micro Hei Mono", Font.BOLD, 16));
		btnLevelThree.setBounds((screenWid/2)-300/2,screenHei/40+(screenHei/3)+50+(screenHei/15), 300, screenHei/15);

		btnLevelThree.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				circus = new WorldLevel_3(readConfig.getWidth(), readConfig.getHeight(), readConfig.getNumOfObjs());
				circus.setLevelProperty();
				circus.intializeField(readConfig.getDISTANCEBETWEENCLOWNS(),readConfig.getFIRSTLINELENGTH(),readConfig.getVERTICALDISTANCEBETNLINES());
				GameController gameController =GameEngine.start("Circus Of Plates", circus,menuBar);
				
				pauseMenuItem.addActionListener(new ActionListener() {
					@Override public void actionPerformed(ActionEvent e) {
						gameController.pause();
					}
				});
				resumeMenuItem.addActionListener(new ActionListener() {
					@Override public void actionPerformed(ActionEvent e) {
						gameController.resume();
					}
				});
			}
		});

		frame.getContentPane().add(btnLevelThree);


	}
}
