package eg.edu.alexu.csd.oop.game.object;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;

import eg.edu.alexu.csd.oop.game.GameObject;

public class ImageObject implements GameObject{
	private static final int MAX_MSTATE = 1;
	// an array of sprite images that are drawn sequentially
	private BufferedImage[] spriteImages = new BufferedImage[MAX_MSTATE];
	private int x;
	private int y;
	private boolean visible;
	private int start , end;
	private JLabel text;
	//private Graphics2D imageG, labelG;
	public void setStart(int start) {
		this.start = start;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public ImageObject(int posX, int posY, String path){
		this.x = posX;
		this.y = posY;
		end = 1500;
		this.visible = true;
		// create a bunch of buffered images and place into an array, to be displayed sequentially
		try {
//			if (path == null){
//				text = new JLabel();
//				text.setBounds(posX, posY, 150, 70);
//			//	g2d = (Graphics2D) text.getGraphics();
//				text.setText("");
//				text.setForeground(Color.orange);
//				text.setFont(new Font("WenQuanYi Micro Hei Mono", Font.BOLD, 20));
//				spriteImages[0] = new BufferedImage(150, 70,BufferedImage.TYPE_INT_ARGB);
//				labelG = (Graphics2D)text.getGraphics();
//				imageG = spriteImages[0].createGraphics();
//				
//			}
//			else
				spriteImages[0] = ImageIO.read(getClass().getResourceAsStream(path));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
//	public void writeScore(String score){
//		text.setText(text.getText()+"			"+score);
//		labelG.drawString(text.getText(),150 , 70);
//		labelG.setColor(Color.orange);
//		labelG.setFont(new Font("WenQuanYi Micro Hei Mono", Font.BOLD, 20));
//		imageG = labelG;
//		Log4j.getInstance().getLogger().debug(text.getText());
//	}
//	public void writeTime(String time){
//		text.setText(text.getText()+"			"+time);
//		
//		labelG.drawString(text.getText(),150 , 70);
//		labelG.setColor(Color.orange);
//		labelG.setFont(new Font("WenQuanYi Micro Hei Mono", Font.BOLD, 20));
//		imageG = labelG;
//		Log4j.getInstance().getLogger().debug(text.getText());
//	}
//	public void clearLabel(){
//		text.setText("");
//
//	}

		public void writeTime(long time){
			Graphics2D g2d = spriteImages[0].createGraphics();
			g2d.setPaint(Color.RED);
			g2d.setFont(new Font("WenQuanYi Micro Hei Mono", Font.BOLD, 14));
			int x = spriteImages[0].getWidth()/2-30;
			int y = (spriteImages[0].getHeight())/2;
			g2d.drawString("Time " + time, x, y);
			g2d.dispose();
		}
//	public void writeWinnigStatement(){
//		text.setText("You WIN!!!");
//		labelG.drawString(text.getText(),150 , 70);
//		labelG.setFont(new Font("WenQuanYi Micro Hei Mono", Font.BOLD, 100));
//		labelG.setColor(Color.orange);
//		imageG = labelG;
//	}
//	public void writeLifeCounter(String life){
//		text.setText(text.getText()+"			"+life);
//		labelG.drawString(text.getText(),150 , 70);
//		labelG.setColor(Color.orange);
//		labelG.setFont(new Font("WenQuanYi Micro Hei Mono", Font.BOLD, 20));
//		imageG = labelG;
//	}
	@Override
	public int getX() {
		return x;
	}

	@Override
	public void setX(int mX) {
		if(mX > start && mX < end)
			this.x = mX;
	}

	public int getStart() {
		return start;
	}

	public int getEnd() {
		return end;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public void setY(int mY) {
		//this.y = mY;
	}


	@Override
	public BufferedImage[] getSpriteImages() {
		return spriteImages;
	}

	@Override
	public int getWidth(){
//		if(spriteImages[0] == null)
//			return text.getWidth();
		return spriteImages[0].getWidth();
	}

	@Override
	public int getHeight() {
		if(spriteImages[0] == null)
			return text.getHeight();
		return spriteImages[0].getHeight();
	}

	@Override
	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible){
		this.visible = visible;
	}
}
