package eg.edu.alexu.csd.oop.game;

import eg.edu.alexu.csd.oop.game.world.Circus;

public class WorldLevel_2 extends Circus{

	public WorldLevel_2(int screenWidth, int screenHeight, int maxNumOfObjs) {

		super(screenWidth, screenHeight, maxNumOfObjs);
		levelHardness = new LevelTwoStrategy();
	}

}
