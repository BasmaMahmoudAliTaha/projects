package eg.edu.alexu.csd.oop.db;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class Deletetion {

	public int deleteData(String tableName , String databaseName , String colName , int mode , LinkedHashMap<String , Object> properties , String operation , String value , String databaseDirectory) throws SQLException{
		int deletedItems = 0;
		switch(mode){
		case 0:
			try {
				deletedItems = xmlModifying(tableName , databaseName , properties, databaseDirectory);
			} catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
				e.printStackTrace();
			}
			break;
		case 1:
			if(!properties.containsKey(colName)){
				throw new RuntimeException();
			}
			try {
				deletedItems = xmlModifying(tableName , databaseName , properties , colName , operation , value, databaseDirectory);
			} catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
				e.printStackTrace();
			}
			break;
		}
		return deletedItems;
	}

	private int xmlModifying(String tableName , String databaseName , LinkedHashMap<String , Object> properties , String colName , String operation , String value , String databaseDirectory) throws ParserConfigurationException, SAXException, IOException, TransformerException, SQLException{
		File file = new File(databaseDirectory+"/"+databaseName+"/"+tableName+".xml");
		if(file.exists()){
			DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
			fac.setValidating(true);
			fac.setNamespaceAware(true);
			DocumentBuilder builder = fac.newDocumentBuilder();
			builder.setErrorHandler(new ErrorHandler() {
				@Override
				public void warning(SAXParseException e) throws SAXException {
					System.out.println("WARNING : " + e.getMessage()); // do nothing
				}
				@Override
				public void fatalError(SAXParseException e) throws SAXException {
					System.out.println("Fatal : " + e.getMessage());
					throw e;
				}
				@Override
				public void error(SAXParseException e) throws SAXException {
					System.out.println("Error : "  );
					e.printStackTrace();
					throw e;
				}
			});
			Document doc = builder.parse(file);
			doc.getDocumentElement().normalize();
			Element root = doc.getDocumentElement();
			NodeList rows = root.getChildNodes();
			int totalChange = 0;
			for(int i = 0 ; i < rows.getLength() ; i++){
				org.w3c.dom.Node row = rows.item(i);
				NodeList cols = row.getChildNodes();
				for(int j = 0 ; j < cols.getLength() ; j++){
					org.w3c.dom.Node curItem = cols.item(j);
					if(curItem.getNodeName().equals(colName) ){
						if(curItem.getTextContent().equals("null") || curItem.getTextContent().equals("max") )
							continue;
						if(operation.equals("=") && curItem.getTextContent().equals(value)){
							root.removeChild(row);
							totalChange++;
							i--;
							break;
						}else if(operation.equals(">")){
							try{
								int num = Integer.parseInt(curItem.getTextContent().trim());
								if(num > Integer.parseInt(value)){
									root.removeChild(row);
									totalChange++;
									i--;
									break;
								}
							}catch(NumberFormatException ex){
								throw new RuntimeException();
							}
						}else if(operation.equals("<")){
							try{
								int num = Integer.parseInt(curItem.getTextContent().trim());
								if(num < Integer.parseInt(value)){
									root.removeChild(row);
									totalChange++;
									i--;
									break;
								}
							}catch(NumberFormatException ex){
								throw new RuntimeException();
							}
						}
					}
				}
			}
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMImplementation domImpl = doc.getImplementation();
			DocumentType doctype = domImpl.createDocumentType(tableName,null,tableName+".dtd");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(file);
			transformer.transform(source, result );
			return totalChange;
		}else
			return 0;
	}

	private int xmlModifying(String tableName , String databaseName , LinkedHashMap<String , Object> properties , String databaseDirectory) throws ParserConfigurationException, SAXException, IOException, TransformerException{
		File file = new File(databaseDirectory+"/"+databaseName+"/"+tableName+".xml");
		if(file.exists()){
			DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
			fac.setValidating(true);
			fac.setNamespaceAware(true);
			DocumentBuilder builder = fac.newDocumentBuilder();
			builder.setErrorHandler(new ErrorHandler() {
				@Override
				public void warning(SAXParseException e) throws SAXException {
					System.out.println("WARNING : " + e.getMessage()); // do nothing
				}
				@Override
				public void fatalError(SAXParseException e) throws SAXException {
					System.out.println("Fatal : " + e.getMessage());
					throw e;
				}
				@Override
				public void error(SAXParseException e) throws SAXException {
					System.out.println("Error : "  );
					e.printStackTrace();
					throw e;
				}
			});
			Document doc = builder.parse(file);
			doc.getDocumentElement().normalize();
			Element root = doc.getDocumentElement();
			NodeList rows = root.getChildNodes();
			int i = 0;
			while(rows.getLength() != 0){
				root.removeChild(rows.item(0));
				i++;
			}
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();

			DOMImplementation domImpl = doc.getImplementation();
			DocumentType doctype = domImpl.createDocumentType(tableName,null,tableName+".dtd");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(file);
			transformer.transform(source, result );
			return i;
		}else
			return 0;
	}
}
