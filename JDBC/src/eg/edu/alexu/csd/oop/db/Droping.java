package eg.edu.alexu.csd.oop.db;

import java.io.File;
import java.io.IOException;

public class Droping {

	public boolean dropDatabase(String databaseName , String databaseDirectory){
		File f = new File(databaseDirectory+File.separator+databaseName);
		boolean success = false;
		if(f.exists()){
			try{
				File[] filesInsideDatabase = f.listFiles();
				for(File file : filesInsideDatabase)
					file.delete();
				if(!f.delete())
					throw new IOException();
				success = true;
			}catch(NullPointerException | IOException ex){
				ex.printStackTrace();
			}
		}
		return success;
	}

	public boolean dropTable(String tableName , String databaseName , String databaseDirectory){
		File f = new File(databaseDirectory+File.separator+databaseName+ File.separator +tableName+".xml");
		File f1 = new File(databaseDirectory+File.separator+databaseName+ File.separator +tableName+".dtd");
		if(!f.exists()){
			return false;
		}
		f.delete();
		f1.delete();
		return true;
	}
}