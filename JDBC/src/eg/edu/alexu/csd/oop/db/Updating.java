package eg.edu.alexu.csd.oop.db;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class Updating {
	
	private String tableName;
	private ArrayList<String> tabels = new ArrayList<String>();
	private ArrayList<LinkedHashMap<String,Object>> properties = new ArrayList<LinkedHashMap<String,Object>>();
	private String dbName;
	private int selectedTable = -1;
	private String[] selectedCols, values;
	private boolean whereExist = false;
	private  int noOfUpdatedRows = 0;
	private String databaseDirectory;

	public Updating(ArrayList<String> tableNames, ArrayList<LinkedHashMap<String,Object>> properties, String dbName ,String databaseDirectory){
		this.tabels = tableNames;
		this.properties = properties;
		this.dbName  = dbName;
		this.databaseDirectory = databaseDirectory;
	}
	private void readDatafromXmlAndReplaceInNodeList(String cond) throws SQLException{
		try {
			String fileNameAndDirec = databaseDirectory+File.separator+dbName+File.separator+tableName+".xml";
			File f = new File(fileNameAndDirec);
			DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
			fac.setValidating(true);
			fac.setNamespaceAware(true);
			DocumentBuilder builder = fac.newDocumentBuilder();
			builder.setErrorHandler(new ErrorHandler() {
				@Override
				public void warning(SAXParseException e) throws SAXException {
					// TODO Auto-generated method stub
					System.out.println("WARNING : " + e.getMessage()); // do nothing
				}
				@Override
				public void fatalError(SAXParseException e) throws SAXException {
					System.out.println("Fatal : " + e.getMessage());
					throw e;
				}
				@Override
				public void error(SAXParseException e) throws SAXException {
					System.out.println("Error : "  );
					e.printStackTrace();
					throw e;
				}
			});
			Document doc = builder.parse(f);
			doc.getDocumentElement().normalize();

			// get document root element
			Element docElem = doc.getDocumentElement();
			NodeList rows = docElem.getChildNodes();
			int rowsNum = rows.getLength();
			for(int i = 0 ; i < rowsNum; i++) {
				Element el = (Element) rows.item(i);
				NodeList cols = el.getChildNodes();
				replaceValues(cond, cols,rowsNum);
			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMImplementation domImpl = doc.getImplementation();
			DocumentType doctype = domImpl.createDocumentType(tableName,null,tableName+".dtd");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(f);
			transformer.transform(source, result );

		}catch(ParserConfigurationException |SAXException |IOException | TransformerException e) {
			throw new RuntimeException("Error reading from xml DB");
		}
	}

	private void replaceValues(String cond, NodeList cols, int rowsNum) throws SQLException{
		if(!whereExist){
			replaceAllRows(cols);
			noOfUpdatedRows = rowsNum;
		}
		else
			replaceSpecificCols(cond, cols);
	}
	// replaces all rows in the selected cols with the determined values 
	private void replaceAllRows(NodeList cols){ //withOut where condition
		for(int i = 0; i < selectedCols.length; i++){
			for(int j = 0; j < cols.getLength(); j++){
				if(cols.item(j).getNodeName().equals(selectedCols[i])){
					cols.item(j).setTextContent(values[i]);
				}
			}
		}
	}
	private void replaceSpecificCols(String cond, NodeList cols) throws SQLException{
		cond = cond.trim();
		String colName = cond.split("=")[0].trim();
		String value = cond.split("=")[1].trim().replaceAll("'", "");
		int colIndex = findDesiredCol(colName, cols);
		String[] s = new String[1];
		s[0] = colName;
		if(colFound(s)){
			if(cols.item(colIndex).getTextContent().replaceAll("'", "").equals(value)){ //in this row we found the specific value
				for(int i = 0; i < selectedCols.length; i++){
					for(int j = 0; j < cols.getLength(); j++){
						if(cols.item(j).getNodeName().equals(selectedCols[i])){
							cols.item(j).setTextContent(values[i]);
						}
					}
				}
				noOfUpdatedRows++;
			}				
		}
		else
			throw new SQLException();
	}

	private boolean colFound(String[] selectedCols){
		int counter = 0;
		for(int k = 0; k < selectedCols.length; k++){
			for(Map.Entry<String,Object> entry : properties.get(selectedTable).entrySet()){
				if(entry.getKey().equals(selectedCols[k])){
					counter++;
				}
			}
		}
		if(counter == selectedCols.length)
			return true;

		return false;
	}

	private int findDesiredCol(String colName, NodeList cols){
		for(int i = 0; i < cols.getLength(); i++){
			if(cols.item(i).getNodeName().equals(colName))
				return i;
		}
		return -1; //error
	}

	private boolean tableNameExist(String s){
		for(String name : tabels){
			selectedTable++;
			if(s.equals(name)){
				return true;
			}
		}
		return false;
	}

	private boolean valuesAppliesToCorrectType(){
		Pattern p; 
		Matcher m;
		for(int i = 0; i < selectedCols.length; i++){
			for(Map.Entry<String, Object> entry : properties.get(selectedTable).entrySet()){
				if(selectedCols[i].equals(entry.getKey())){
					if(entry.getValue().equals("max")){ //int value
						p = Pattern.compile("-?\\d+");
						m = p.matcher(values[i]);
						if(m.matches()); //is int (true)

						else 
							return false;
					}
					else if(entry.getValue().equals("null")){//varchar
						p = Pattern.compile("\\w+");
						m = p.matcher(values[i]);
						if(m.matches()); // is string (true)

						else
							return false;

					}
				}
			}
		}
		return true;
	}

	public int doUpdating(String query) throws SQLException{
		Pattern p1  = Pattern.compile("where(.+)");
		Pattern p2  = Pattern.compile("update\\s+(\\w+)\\s+set(.+)");
		Matcher  m1 = p1.matcher(query);
		Matcher  m2 = p2.matcher(query);
		if(m1.find()){
			whereExist = true;
			p2  = Pattern.compile("update\\s+(\\w+)\\s+set(.+)\\s+where");
			m2 = p2.matcher(query);
		}
		if(m2.find()){
			tableName = m2.group(1);

			if(tableNameExist(tableName)){

				String[] sets = m2.group(2).split(",");
				selectedCols = new String[sets.length];
				values = new String[sets.length];
				for( int i = 0; i < sets.length; i++){
					selectedCols[i] = sets[i].split("=")[0].trim();
					values[i] = sets[i].split("=")[1].replaceAll("'", "").trim();
				}
				if(colFound(selectedCols)){
					if(valuesAppliesToCorrectType()){
						String cond = "";
						if(whereExist){
							p1  = Pattern.compile("where(.+)");
							m1 = p1.matcher(query);
							if(m1.find()){
								cond = m1.group(1);
							}
						}
						readDatafromXmlAndReplaceInNodeList(cond);
						return noOfUpdatedRows;
					}
					else
						throw new SQLException();
				}
				else
					throw new SQLException();
			}
			else
				throw new SQLException();
		}
		else
			throw new SQLException();
	}
}