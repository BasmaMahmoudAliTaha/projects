package eg.edu.alexu.csd.oop.db;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class DatabaseEngine implements Database {

	private Parser parse;
	private boolean mode = false;
	public DatabaseEngine() {
		parse = new Parser();
	}
	
	public DatabaseEngine(String databaseDirectory) {
		parse = new Parser(databaseDirectory);
	}
	
	@Override
	public String createDatabase(String databaseName, boolean dropIfExists) {
		try {
			mode = true;
			boolean flag = executeStructureQuery("CREATE DATABASE "+ databaseName.trim());
			if(!flag && dropIfExists)
			{
				executeStructureQuery("DROP DATABASE "+ databaseName);
				executeStructureQuery("CREATE DATABASE "+ databaseName);
			}
			// side note if dropIfexists = false
			mode = false;
			return (System.getProperty("user.dir")+ File.separator + databaseName);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean executeStructureQuery(String query) throws SQLException {
		boolean temp = parse.parseInputCreation(query ,mode);
		return temp;
	}

	@Override
	public Object[][] executeQuery(String query) throws SQLException {
		return parse.parseInputSelection(query);
	}

	@Override
	public int executeUpdateQuery(String query) throws SQLException {

		return parse.parseInputModification(query);

	}

	public LinkedHashMap<String,Object> getProperties(String tableName){
		ArrayList<String> tabels = parse.getTables();
		ArrayList<LinkedHashMap<String,Object>> properties = parse.getProperties();
		return properties.get(tabels.indexOf(tableName));
	}
	
}
