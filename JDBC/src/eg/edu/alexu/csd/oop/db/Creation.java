package eg.edu.alexu.csd.oop.db;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;


public class Creation {

	public boolean createDatabase(String databaseName, boolean mode , String databaseDirectory){
		File file = new File(databaseDirectory+File.separator+databaseName);
		if(file.exists() ){
			if(!mode)
				return false;
			else{
				Droping drop = new Droping();
				drop.dropDatabase(databaseName , databaseDirectory);
			}
		}
		file.mkdirs();
		return true;
	}

	public LinkedHashMap<String , Object>  createTable(String query , String tableName , String databaseName , String databaseDirectory) throws SQLException{
		if(databaseName == null)
			throw new RuntimeException();
		File file = new File(databaseDirectory+File.separator+databaseName+File.separator+tableName+".xml");
		if(file.exists()){
			return null;
		}
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		// Pattern pat = Pattern.compile("(\\w+)\\s+(int|varchar)");
		Pattern pat = Pattern.compile("\\s*create\\s+table\\s+(\\w+)\\s*\\(\\s*((\\w+)\\s+(int|varchar)\\s*,\\s*)*(\\w+\\s+(int|varchar))\\s*\\)\\s*");
		Matcher test = pat.matcher(query);
		Pattern pat1 = Pattern.compile("(\\w+)\\s+(int|varchar)");
		Matcher mat = pat1.matcher(query);
		LinkedHashMap<String , Object> colsInTable = null; 

		if(test.matches()){
			colsInTable = new LinkedHashMap<String , Object>(); 
			while(mat.find()){
				if(mat.group(2).equals("int"))
					colsInTable.put( mat.group(1) , "max" );
				else if(mat.group(2).equals("varchar"))
					colsInTable.put( mat.group(1) , "null" );
			}
			try {
				intializeXml(tableName , databaseName , databaseDirectory);
			} catch (ParserConfigurationException | TransformerException e1) {
				e1.printStackTrace();
			}
			try {
				intializeDTD(colsInTable , tableName , databaseName , databaseDirectory);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			file.delete();
			throw new SQLException();
		}
		return colsInTable;

	}

	private void intializeDTD(LinkedHashMap<String , Object> m , String tableName , String databaseName , String databaseDirectory) throws IOException{
		File dtd = new File(databaseDirectory+"/"+databaseName+"/"+tableName+".dtd");
		dtd.createNewFile();
		FileWriter fw = new FileWriter(dtd);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write("<!ELEMENT " + tableName + " (element)*>\n");
		bw.write("<!ELEMENT element ( ");
		Iterator<String> it = m.keySet().iterator();
		ArrayList<String> keysInmap = new ArrayList<String>();
		while(it.hasNext()){
			keysInmap.add(it.next());
			bw.write(keysInmap.get(keysInmap.size()-1));
			if(it.hasNext())
				bw.write(" , ");
		}
		bw.write(")>\n");
		for(String s : keysInmap){
			bw.write("<!ELEMENT " + s +" (#PCDATA)>\n");
		}
		bw.close();
	}

	private void intializeXml(String tableName , String databaseName , String databaseDirectory) throws ParserConfigurationException, TransformerException{
		File file = new File(databaseDirectory+"/"+databaseName+"/"+tableName+".xml");
		if(file.exists()){
			DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = fac.newDocumentBuilder();		
			Document doc = builder.newDocument();
			Element root = doc.createElement(tableName);
			doc.appendChild(root);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMImplementation domImpl = doc.getImplementation();
			DocumentType doctype = domImpl.createDocumentType(tableName,null,tableName+".dtd");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(file);
			transformer.transform(source, result );
		}
	}
}