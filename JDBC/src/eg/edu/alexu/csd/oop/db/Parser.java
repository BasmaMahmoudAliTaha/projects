package eg.edu.alexu.csd.oop.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

	private boolean canStart = false;
	private ArrayList<String> tabels = new ArrayList<String>();
	private ArrayList<LinkedHashMap<String,Object>> properties = new ArrayList<LinkedHashMap<String,Object>>();
	private String currentDatabaseName = null;
	private String databaseDirectory = null;
	
	public Parser(){}
	
	public Parser(String databaseDirectory){
		this.databaseDirectory = databaseDirectory;
	}
	
	public ArrayList<String> getTables(){
		return tabels;
	}
	
	public ArrayList<LinkedHashMap<String,Object>> getProperties(){
		return properties;
	}
	
	public boolean parseInputCreation(String query , boolean mode) throws SQLException{
		query = query.toLowerCase();
		Pattern pat = Pattern.compile("^\\s*(create|drop)\\s+");
		Matcher mat = pat.matcher(query);
	
		//if(mat.groupCount() == 1){
		if(mat.find()){
			//throw new SQLException();
			String cur = mat.group().trim();
			boolean success = false;

			if(cur.equals("create"))
				success = createData(query , mode);
			else if(cur.equals("drop"))
				success = dropData(query );

			return success;
		}else
			throw new RuntimeException("error");
	}

	public Object[][] parseInputSelection(String query) throws SQLException{
		query = query.toLowerCase();
		Pattern ver1 = Pattern.compile("^\\s*select\\s+((\\w+)\\s*,\\s*)*(\\w+)\\s+from\\s+\\w+\\s*");
		Matcher m1 = ver1.matcher(query);
		Pattern ver2 = Pattern.compile("^\\s*select\\s*\\*\\s+from\\s+(\\w+)\\s*");
		Matcher m2 = ver2.matcher(query);
		//ver1 Where with list of cols
		Pattern ver3Test1 = Pattern.compile("^\\s*select\\s+(\\w+\\s*,\\s*)*(\\w+)\\s+from\\s+(\\w+)\\s+where\\s+(\\w+)\\s*(=|<|>)\\s*(\\'\\s*)?(\\w+)(\\')?\\s*");
		//ver2 where with all table
		Pattern ver3Test2 = Pattern.compile("^\\s*select\\s*\\*\\s+from\\s+(\\w+)\\s+where\\s+((\\w+)\\s*(=|<|>)\\s*(\\'\\s*)?(\\w+))(\\')?\\s*");
		Matcher m3Test1 = ver3Test1.matcher(query); //matcher ver1
		Matcher m3Test2 = ver3Test2.matcher(query); //matcher ver2
		Selection select = new Selection(tabels, properties, currentDatabaseName, databaseDirectory );
		if(m1.matches()||m2.matches()||m3Test1.matches()||m3Test2.matches()){
			Object[][] record  = {};
			query = query.trim();
			if(canStart){
				if(m1.matches())
					record = select.selectVer1(query);
				else if(m2.matches())
					record = select.selectVer2(query);
				else if(m3Test1.matches())
					record = select.selectVer3(query,false);
				else if(m3Test2.matches())
					record = select.selectVer3(query,true);

				return record;
			}
			else // there is no dataBase available
				throw new RuntimeException();
		}
		else
			throw new SQLException();
	}


	public int parseInputModification(String query) throws SQLException{
		query = query.toLowerCase();
		Pattern pat = Pattern.compile("^\\s*(update|delete|insert)\\s+");
		Matcher mat = pat.matcher(query);
		//if(mat.groupCount() == 1){
		if(mat.find()){
			//throw new SQLException();
			String cur = mat.group().trim();
			int success = 0;

			if(canStart){
				if(cur.equals("update")){
					Pattern p = Pattern.compile("^\\s*update\\s+(\\w+)\\s+set\\s+((\\w+)\\s*=\\s*(\\'\\s*)?(\\w+)\\s*(\\'\\s*)?,\\s*)*((\\w+)\\s*=\\s*(\\'\\s*)?(\\w+)\\s*(\\')?)\\s*(where\\s+((\\w+)\\s*=\\s*(\\'\\s*)?(\\w+)(\\'\\s*)?)\\s*)?");
					Matcher m = p.matcher(query);
					if(m.matches()){
						Updating update = new Updating(tabels, properties, currentDatabaseName , databaseDirectory);
						query = query.trim();
						success = update.doUpdating(query);
					}
				}
				else if(cur.equals("delete"))
					success = deleteData(query);
				else if(cur.equals("insert"))
					success = insertData(query);
				
				return success ;
			}else
				throw new RuntimeException();
		}
		throw new SQLException();
	}

	private boolean createData(String query , boolean mode) throws SQLException{
		Pattern pat = Pattern.compile("^\\s*create\\s+(database|table)\\s+(\\w+)");
		Matcher mat = pat.matcher(query);
		//if(mat.groupCount() == 2){
		if(mat.find()){
			//throw new SQLException();
			String cur = mat.group(1);
			Creation create = new Creation();
			boolean success = false;
			if(cur.equals("database")){
				String databaseName = mat.group(2);
				success = create.createDatabase(databaseName , mode , databaseDirectory);
				if(success){
					currentDatabaseName = databaseName;
					canStart = true;
				}
			}
			else if(cur.equals("table") && canStart){
				Pattern pat1 = Pattern.compile("\\s*create\\s+table\\s+"+mat.group(2).trim()+"\\s*\\(.*\\)\\s*");
				Matcher mat1 = pat1.matcher(query);
				if(!mat1.find()){
					throw new SQLException();
				}
				LinkedHashMap<String , Object> colsInTable = create.createTable(query , mat.group(2) , currentDatabaseName , databaseDirectory);
				if(colsInTable != null){
					success = true;
					properties.add(colsInTable);
					tabels.add(mat.group(2));
				}
			}else
				throw new RuntimeException();
			return success;
		}else
			throw new SQLException();
	}

	private boolean dropData(String query) throws SQLException{
		Pattern pat = Pattern.compile("^\\s*drop\\s+(database|table)\\s+(\\w+)");
		Matcher mat = pat.matcher(query);
		//if(mat.groupCount() == 2){
		
		if(mat.find()){
			//throw new SQLException();
			String cur = mat.group(1);
			Droping drop = new Droping();
			boolean success = false;
			if(cur.equals("database")){
				success = drop.dropDatabase(mat.group(2) , databaseDirectory);
				if(success){
					currentDatabaseName = null;
					canStart = false;
				}
			}
			else if(cur.equals("table") && canStart){
				success = drop.dropTable(mat.group(2) , currentDatabaseName , databaseDirectory);
				if(success){
					int index = tabels.indexOf(mat.group(2));
					properties.remove(index);
					tabels.remove(index);
					for(String s : tabels)
						System.out.println(s);
				}
			}else
				throw new RuntimeException();
			return success;
		}
		throw new SQLException();
	}

	private int deleteData(String query) throws SQLException{
		Pattern pat = Pattern.compile("\\s*delete\\s+\\*?\\s*from\\s+(\\w+)\\s*");
		Matcher mat = pat.matcher(query);
		Pattern pat1 = Pattern.compile("\\s*delete\\s+from\\s+(\\w+)\\s+where\\s+(\\w+)\\s*(=|>|<)\\s*['|\"]?(\\w+)['|\"]?\\s*");
		Matcher mat1 = pat1.matcher(query);
		Deletetion delete = new Deletetion();
		int numOfdeleted = 0;
		if(mat1.matches()){
			try{
				numOfdeleted = delete.deleteData(mat1.group(1), currentDatabaseName, mat1.group(2), 1,  properties.get(tabels.indexOf(mat1.group(1))) , mat1.group(3) , mat1.group(4) , databaseDirectory);
			}catch(ArrayIndexOutOfBoundsException ex){
				throw new RuntimeException();
			}
		}
		else if(mat.matches()){
			try{
				numOfdeleted = delete.deleteData(mat.group(1), currentDatabaseName, null, 0 , properties.get(tabels.indexOf(mat.group(1))) , null , null , databaseDirectory);
			}catch(ArrayIndexOutOfBoundsException ex){
				throw new RuntimeException();
			}
		}else{
			throw new SQLException();
		}
		return numOfdeleted;

	}


	private int insertData(String query) throws SQLException{
		Pattern pat = Pattern.compile("\\s*insert\\s+into\\s+(\\w+)\\s+values\\s*\\(\\s*.*\\s*\\)\\s*");
		Matcher mat = pat.matcher(query);
		Pattern pat2 = Pattern.compile("\\s*insert\\s+into\\s+(\\w+)\\s*\\(\\s*.*\\s*\\)\\s*values\\s*\\(\\s*.*\\s*\\)\\s*");
		Matcher mat2 = pat2.matcher(query);
		Insertion insert = new Insertion();
		int numberOfinsert = 0;
		if(mat.find()){
			try{
				numberOfinsert = insert.insertData(mat.group(1), currentDatabaseName, query, 0 , properties.get(tabels.indexOf(mat.group(1))) , databaseDirectory); 	
			}catch(ArrayIndexOutOfBoundsException ex){
				throw new RuntimeException();
			}
		}else if(mat2.find()){
			try{
				numberOfinsert = insert.insertData(mat2.group(1), currentDatabaseName, query, 1 , properties.get(tabels.indexOf(mat2.group(1))) , databaseDirectory); 	
			}catch(ArrayIndexOutOfBoundsException ex){
				throw new RuntimeException();
			}
		}else
			throw new SQLException();
		return numberOfinsert;

	}
}
