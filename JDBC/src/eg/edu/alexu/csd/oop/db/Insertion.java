package eg.edu.alexu.csd.oop.db;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class Insertion {

	public int insertData(String tableName , String databaseName , String query , int mode , LinkedHashMap<String , Object> properties , String databaseDirectory) throws SQLException{
		LinkedHashMap< String , Object> keys = new LinkedHashMap<String , Object>(properties);
		int insertedItems = 0;
		switch(mode){
		case 0:{
			Pattern pat = Pattern.compile("\\(.*\\)");
			Matcher mat = pat.matcher(query);
			String s[] = null;
			if(mat.find())
				s = mat.group().substring(1,mat.group().length()-1).split(",");
			Iterator<String> it = properties.keySet().iterator();
			int counter = 0 ;
			while(counter < s.length && it.hasNext()){
				String cur = it.next();
				if(keys.get(cur).equals("max")){
					try{
						keys.put(cur, Integer.parseInt(s[counter].trim()));
					}catch(NumberFormatException ex){
						throw new RuntimeException();
					}
				}else{
					String prop = s[counter].trim();
					if(prop.startsWith("'") || prop.startsWith("\""))
						prop = prop.substring(1,prop.length()-1);
					keys.put(cur, prop);
				}
				counter++;
			}
			if(counter != properties.size())
				throw new RuntimeException();
			break;
		}
		case 1:{
			Pattern pat1 = Pattern.compile("\\s*insert\\s+into\\s"+tableName+"\\s*\\(\\s*((\\w+)\\s*,\\s*)*(\\s*\\w+\\s*)*\\s*\\)\\s*values\\s*\\(\\s*(\\s*['|\"]?\\s*(\\w+)\\s*['|\"]?\\s*,\\s*)*(\\s*['|\"]?\\s*\\w+\\s*['|\"]?\\s*)*\\s*\\)\\s*");
			Matcher test = pat1.matcher(query);
			Pattern pat = Pattern.compile("\\(\\s*(.+?[^\\)])\\s*\\)\\s*values\\s*\\(\\s*(.+[^\\)])\\s*\\)\\s*");
			Matcher mat = pat.matcher(query);
			String[] firstGroup = null;
			String[] secondGroup = null;
			if(test.matches()){
				if(mat.find()){
					firstGroup = mat.group(1).split(",");
					secondGroup = mat.group(2).split(",");
				}try{
					if(!(firstGroup == null && secondGroup == null) && (firstGroup.length != secondGroup.length)){
						throw new RuntimeException();
					}
				}catch(NullPointerException ex){
					throw new RuntimeException();
				}
				int counter = 0 ;
				while(firstGroup != null && counter < firstGroup.length){
					if(keys.containsKey(firstGroup[counter].trim())){
						if(keys.get(firstGroup[counter].trim()).equals("max")){
							try{
								keys.put(firstGroup[counter].trim(), Integer.parseInt(secondGroup[counter].trim()));
							}catch(NumberFormatException ex){
								ex.printStackTrace();
								throw new RuntimeException();
							}
						}else{
							String prop = secondGroup[counter].trim();
							if(prop.startsWith("'") || prop.startsWith("\""))
								prop = prop.substring(1,prop.length()-1);
							keys.put(firstGroup[counter].trim(),prop);
						}
					}else
						throw new RuntimeException();
					counter++;
				}
			}else
				throw new SQLException();
		}
		}
		try {
			insertedItems = xmlModifying(tableName , databaseName , keys , databaseDirectory);
		} catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
			e.printStackTrace();
		}
		return insertedItems;
	}

	private int xmlModifying(String tableName , String databaseName , LinkedHashMap<String , Object> properties , String databaseDirectory) throws ParserConfigurationException, SAXException, IOException, TransformerException{
		File f = new File(databaseDirectory+"/"+databaseName+"/"+tableName+".xml");
		if(f.exists()){
			DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
			fac.setValidating(true);
			fac.setNamespaceAware(true);
			DocumentBuilder builder = fac.newDocumentBuilder();
			builder.setErrorHandler(new ErrorHandler() {
				@Override
				public void warning(SAXParseException e) throws SAXException {
					// TODO Auto-generated method stub
					System.out.println("WARNING : " + e.getMessage()); // do nothing
				}
				@Override
				public void fatalError(SAXParseException e) throws SAXException {
					System.out.println("Fatal : " + e.getMessage());
					throw e;
				}
				@Override
				public void error(SAXParseException e) throws SAXException {
					System.out.println("Error : "  );
					e.printStackTrace();
					throw e;
				}
			});
			Document doc = builder.parse(f);
			doc.getDocumentElement().normalize();
			Element root = doc.getDocumentElement();
			Element child = doc.createElement("element");
			root.appendChild(child);
			Iterator<String> it = properties.keySet().iterator();
			while(it.hasNext()){
				String next = it.next();
				Element col = doc.createElement(next);
				if(properties.get(next) instanceof Integer)
					col.setTextContent(String.valueOf(properties.get(next)));
				else
					col.setTextContent((String)properties.get(next));
				child.appendChild(col);
			}
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			//transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			//transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			DOMImplementation domImpl = doc.getImplementation();
			DocumentType doctype = domImpl.createDocumentType(tableName,null,tableName+".dtd");
			//transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, doctype.getPublicId());
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(f);
			transformer.transform(source, result );
			return 1;
		}else
			return 0;
	}
}
