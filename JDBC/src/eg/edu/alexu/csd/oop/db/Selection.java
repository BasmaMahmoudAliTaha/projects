package eg.edu.alexu.csd.oop.db;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Selection {
	private ArrayList<String> tabels = new ArrayList<String>();
	private ArrayList<LinkedHashMap<String,Object>> properties = new ArrayList<LinkedHashMap<String,Object>>();
	private int selectedTable = -1;
	private Object[][] records = new Object[1000][1000];
	private String selectedTableName, dbName;
	private boolean allTableWithWhere = false, atLeastOneElementRecordedInARow = false;
	private int  j = 0, l = 0; // indices for records
	private String databaseDirectory;
	
	public Selection(ArrayList<String> tableNames, ArrayList<LinkedHashMap<String,Object>> properties, String dbName , String databaseDirectory){
		this.tabels = tableNames;
		this.properties = properties;
		this.dbName  = dbName;
		this.databaseDirectory = databaseDirectory;
	}


	private void selectFromXML(String[] selectedCols,String whereCond, boolean ver1, boolean ver2, boolean ver3) throws SQLException{
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(); 

			String fileNameAndDirec = databaseDirectory+File.separator+dbName+File.separator+selectedTableName+".xml";
			//Using factory get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();
			File f = new File(fileNameAndDirec);
			//parse using builder to get DOM representation of the XML file
			Document doc = db.parse(f);

			// get document root element
			Element docElem = doc.getDocumentElement();
			NodeList rows = docElem.getChildNodes();

			if(ver1 && colFound(selectedCols)){
				for(int i = 0 ; i < rows.getLength();i++) {
					Element el =  (Element)rows.item(i);
					NodeList cols = el.getChildNodes();
					recordSelectedCols(selectedCols, cols);	
					if(atLeastOneElementRecordedInARow)
						l++;
					if(i!=rows.getLength()-1)
						j = 0;
					else
						j = selectedCols.length;
					atLeastOneElementRecordedInARow = false;

				}
			}
			else if(ver2 && !ver1){
				for(int i = 0 ; i < rows.getLength();i++) {
					Element el = (Element)rows.item(i);
					NodeList cols = el.getChildNodes();
					recordAllTable(cols);
					if(atLeastOneElementRecordedInARow)
						l++;
					if(i!=rows.getLength()-1)
						j = 0;
					else
						j = cols.getLength();
					atLeastOneElementRecordedInARow = false;
				}
			}
			else if(ver3){
				if(allTableWithWhere){
					for(int i = 0 ; i < rows.getLength();i++) {
						Element el = (Element) rows.item(i);
						NodeList cols = el.getChildNodes();
						recordSelectedColsWhereCond(selectedCols, cols, whereCond);						
						if(atLeastOneElementRecordedInARow)
							l++;
						if(i!=rows.getLength()-1)
							j = 0;
						else
							j = cols.getLength();
						atLeastOneElementRecordedInARow = false;
					}
				}
				else{
					if(colFound(selectedCols)){
						for(int i = 0 ; i < rows.getLength();i++) {
							Element el = (Element) rows.item(i);
							//		if (el.getNodeType() == Node.ELEMENT_NODE) {
							NodeList cols = el.getChildNodes();
							recordSelectedColsWhereCond(selectedCols, cols, whereCond);						
							if(atLeastOneElementRecordedInARow)
								l++;
							if(i!=rows.getLength()-1)
								j = 0;
							else
								j = selectedCols.length;
							atLeastOneElementRecordedInARow = false;
						}
					}
				}


			}
			else
				throw new SQLException();

		}catch(ParserConfigurationException |SAXException |IOException e) {
			throw new RuntimeException("Error reading from xml DB");
		}
	}

	private boolean colFound(String[] selectedCols){
		int counter = 0;
		for(int k = 0; k < selectedCols.length; k++){
			for(Map.Entry<String,Object> entry : properties.get(selectedTable).entrySet()){
				if(entry.getKey().equals(selectedCols[k])){
					counter++;
				}
			}
		}
		if(counter == selectedCols.length)
			return true;

		return false;
	}
	
	private void recordSelectedCols(String[] selectedCols, NodeList cols){
		for(int k = 0; k < selectedCols.length; k++){
			for(Map.Entry<String,Object> entry : properties.get(selectedTable).entrySet()){
				if(entry.getKey().equals(selectedCols[k])){
					for(int i = 0; i < cols.getLength(); i++){
						if(cols.item(i).getNodeName().equals(selectedCols[k])){
							if(entry.getValue().equals("max")){ // integer col
								if(cols.item(i).getTextContent().equals("max")){//no value recorded in table
									records[l][j] = "";
								}
								else{
									records[l][j] = (int) Integer.parseInt(cols.item(i).getTextContent().trim());
									j++;
								}
								atLeastOneElementRecordedInARow = true;
								break;///// as there is no duplicated names for cols
							}

							else if(entry.getValue().equals("null")){ // varchar col
								if(cols.item(i).getTextContent().equals("null"))
									records[l][j] = "";
								else{
									records[l][j] = (String) cols.item(i).getTextContent().trim();
									j++;
								}
								atLeastOneElementRecordedInARow = true;
								break;///// as there is no duplicated names for cols
							}
						}
					}					
					break;///// as there is no duplicated names for cols
				}
			}
		}
	}

	private void recordAllTable(NodeList cols){
		for(int i = 0; i < cols.getLength(); i++){
			for(Map.Entry<String,Object> entry : properties.get(selectedTable).entrySet()){
				if(cols.item(i).getNodeName().equals(entry.getKey())){
					if(entry.getValue().equals("max")){ // integer col
						if(cols.item(i).getTextContent().equals("max"))
							records[l][j] = "";
						else{
							records[l][j] = (int)Integer.parseInt(cols.item(i).getTextContent());
							j++;
						}
						atLeastOneElementRecordedInARow = true;
						break;
					}

					else if(entry.getValue().equals("null")){ // varchar col
						if(cols.item(i).getTextContent().equals("null"))
							records[l][j] = "";
						else{
							records[l][j] = (String) cols.item(i).getTextContent().trim();
							j++;
						}
						atLeastOneElementRecordedInARow = true;
						break;
					}
				}
			}
		}
	}

	private void recordSelectedColsWhereCond(String[] selectedCols, NodeList cols, String cond) throws SQLException{
		cond = cond.trim();
		Pattern p = Pattern.compile("(=|>|<)");
		Matcher m = p.matcher(cond);
		String operation = null;
		if(m.find()){
			operation = m.group(1);
			String colName = cond.split("(=|>|<)")[0].trim();
			String value = cond.split("(=|>|<)")[1].trim().replaceAll("'", "");
			int colIndex = findDesiredCol(colName, cols);
			String[] s = new String[1];
			s[0] = colName;
			if(colFound(s)){
				if(operation.equals("=")){
					System.out.println(cols.item(colIndex).getTextContent().toLowerCase()+" "+ value.toLowerCase());
					if(cols.item(colIndex).getTextContent().toLowerCase().replaceAll("'", "").equals(value.toLowerCase())){
						if(!allTableWithWhere)
							recordSelectedCols(selectedCols, cols);
						else
							recordAllTable(cols);
					}
				}
				else if(operation.equals("<")){
					System.out.println(cols.item(colIndex).getTextContent());
					if(Integer.parseInt(cols.item(colIndex).getTextContent().replaceAll("'", "")) < Integer.parseInt(value)){
						if(!allTableWithWhere)
							recordSelectedCols(selectedCols, cols);
						else
							recordAllTable(cols);
					}
				}
				else if(operation.equals(">")){
					if(Integer.parseInt(cols.item(colIndex).getTextContent().replaceAll("'", "")) > Integer.parseInt(value)){
						if(!allTableWithWhere)
							recordSelectedCols(selectedCols, cols);
						else
							recordAllTable(cols);
					}
				}
			}
			else
				throw new SQLException();
		}
		else
			throw new SQLException();
	}

	private int findDesiredCol(String colName, NodeList cols){
		for(int i = 0; i < cols.getLength(); i++){
			if(cols.item(i).getNodeName().equals(colName))
				return i;
		}
		return -1; //error
	}

	private boolean tableNameExist(String s){
		for(String name : tabels){
			selectedTable++;
			if(s.equals(name)){
				return true;
			}
		}
		return false;
	}
	private Object[][] trimRecords(){
		Object[][] finalResult = new Object[l][j];
		for(int i  = 0; i < l; i++){
			for(int k = 0; k < j; k++){
				if(records[i][k]!=null){
					finalResult[i][k] = records[i][k];
				}
			}
		}
		if(l == 0 && j == 0){
			return new Object[][] {};
		}
		else
			return finalResult;
	}
	// SELECT column_name(s)FROM table_name
	public Object[][] selectVer1(String query) throws SQLException{
		Pattern p = Pattern.compile("select\\s+(.+)\\s+from\\s+(\\w+)");
		Matcher m = p.matcher(query);
		String[] selectedCols;
		if(m.find()){
			if(m.group(1).contains(",")){
				selectedCols = m.group(1).split(",");

				for(int i = 0; i < selectedCols.length; i++){
					selectedCols[i] = selectedCols[i].trim();
				}
			}

			else{
				selectedCols= new String[1];
				selectedCols[0] = m.group(1).trim();
			}
			selectedTableName = m.group(2);
			if(tableNameExist(selectedTableName)){
				selectFromXML(selectedCols, "", true, false, false);
				return trimRecords() ;
			}
			else
				throw new SQLException();
		}
		else 
			throw new SQLException();
	}

	// SELECT * FROM table_name
	public Object[][] selectVer2(String query) throws SQLException{
		Pattern p = Pattern.compile("^\\s*select\\s*\\*\\s+from\\s+(\\w+)");
		Matcher m = p.matcher(query);
		if(m.find()){
			selectedTableName = m.group(1).trim();
			if(tableNameExist(selectedTableName)){
				selectFromXML(new String[] {}, "", false, true, false);
				return trimRecords();
			}
			else
				throw new SQLException();
		}
		else
			throw new SQLException();
	}

	public Object[][] selectVer3(String query, boolean allTable) throws SQLException{
		Pattern p = Pattern.compile("select(\\s*\\*)?\\s+(.+)\\s+from\\s+(\\w+)\\s+where\\s+(.+)");
		Matcher m = p.matcher(query);
		if(!allTable){
			// determine selectedCols, table name and condition
			String[] selectedCols;
			if(m.find()){
				if(m.group(2).contains(",")){
					selectedCols = m.group(2).split(",");
					for(int i = 0; i < selectedCols.length; i++){
						selectedCols[i] = selectedCols[i].trim();
					}
				}
				else{
					selectedCols= new String[1];
					selectedCols[0] = m.group(2).trim();
				}
				selectedTableName = m.group(3).trim();
				if(tableNameExist(selectedTableName)){
					String cond = m.group(4).trim();
					selectFromXML(selectedCols, cond, false, false, true);
					return trimRecords();
				}
				else
					throw new SQLException();
			}
			else 
				throw new SQLException();
		}
		else{
			// table name and condition
			allTableWithWhere = true;
			if(m.find()){
				selectedTableName = m.group(3).trim();
				if(tableNameExist(selectedTableName)){
					String cond = m.group(4).trim();
					selectFromXML(new String[] {}, cond, false, false, true);
					return trimRecords();
				}
				else
					throw new SQLException();
			}
			else
				throw new SQLException();
		}
	}
}
