package eg.edu.alexu.csd.oop.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.RuntimeErrorException;

import eg.edu.alexu.csd.oop.db.DatabaseEngine;

public class StatementAdapterEngine extends StatementEngine{
	private Connection connection;
	private List<String> batch;
	private DatabaseEngine database;
	private boolean canStart;
	
	public StatementAdapterEngine(Connection connection , DatabaseEngine database) {
		this.connection = connection;
		batch = new ArrayList<String>();
		this.database = database;
		canStart = false;
	}

	@Override
	public Connection getConnection() throws SQLException {
		return connection;
	}

	@Override
	public int[] executeBatch() throws SQLException {
		List<Integer> batchResult = new ArrayList<Integer>();
		if(database != null )
		{
			for(String s : batch){
				String query = s.toLowerCase();
				Pattern pat = Pattern.compile("^\\s*(create|drop|select|update|delete|insert)\\s+");
				Matcher mat = pat.matcher(query);
				if(mat.find()){
					String cur = mat.group().trim();
					//int success = 0;
					if(cur.equals("create")){
						boolean flag = database.executeStructureQuery(query);
						if(flag){
							batchResult.add(1);
							canStart = true;
						}
					}else if(cur.equals("drop")){
						boolean flag = database.executeStructureQuery(query);
						if(flag){
							batchResult.add(1);
							canStart = false;
						}
					}
					else if((cur.equals("update") || cur.equals("delete") || cur.equals("insert")) && canStart){
						int returnedValue = database.executeUpdateQuery(query);
						batchResult.add(returnedValue);
					}else if(cur.equals("select") && canStart){
						Object[][] returnedValue = database.executeQuery(query);
						if(returnedValue.length != 0)
							batchResult.add(1);
					}else
						throw new RuntimeErrorException(null);
				}
			}
		}else
			throw new SQLException();
		int [] finalBatchResult = new int[batchResult.size()];
		int i =0;
		for(Integer num : batchResult)
			finalBatchResult[i++] = num;
		return finalBatchResult;
	}

	@Override
	public void addBatch(String sql) throws SQLException {
		if(connection != null)
			batch.add(sql);
		else
			throw new SQLException();
	}

	@Override
	public void clearBatch() throws SQLException {
		if(connection != null)
			batch.clear();
		else
			throw new SQLException();
	}

	@Override
	public ResultSet executeQuery(String sql) throws SQLException {

		sql = sql.toLowerCase();
		Pattern pat = Pattern.compile("^\\s*(create|drop|select|update|delete|insert)\\s+.+\\s+from\\s+(\\w+)");
		Matcher mat = pat.matcher(sql);
		Object [][] data = null ;
		String tableName = null ;
		LinkedHashMap<String, Object> tableProperty = null;
		if(mat.find()){
			String cur = mat.group(1).trim();
			tableName = mat.group(2).trim();
			tableProperty = database.getProperties(tableName);
			if(cur.equals("select")){
				data = database.executeQuery(sql);
			}
			if(tableName != null && tableProperty != null && data != null)
				return new ResultSetAdabterEngine(data, tableProperty, tableName, this);
			else
				throw new SQLException();

		}
		return new ResultSetAdabterEngine(new Object[][]{}, tableProperty, tableName, this);
	}

	@Override
	public int executeUpdate(String sql) throws SQLException {

		sql = sql.toLowerCase();
		Pattern pat = Pattern.compile("^\\s*(create|drop|select|update|delete|insert)\\s+");
		Matcher mat = pat.matcher(sql);
		int success = 0;
		if(mat.find()){
			String cur = mat.group().trim();
			if((cur.equals("update") || cur.equals("delete") || cur.equals("insert")) && canStart){
				success = database.executeUpdateQuery(sql);
			}else if(cur.equals("create")){
				boolean flag = database.executeStructureQuery(sql);
				if(flag){
					success = 1;
					canStart = true;
				}
			}else if(cur.equals("drop")){
				boolean flag = database.executeStructureQuery(sql);
				if(flag){
					success = 1;
					canStart = false;
				}
			}else if(cur.equals("select") && canStart){
				Object[][] returnedValue = database.executeQuery(sql);
				if(returnedValue.length != 0)
					success = 1;
			}else{
				throw new SQLException();
			}
		}
		return success;
	}

	@Override
	public boolean execute(String sql) throws SQLException {

		
		String query = sql.toLowerCase();
		Pattern pat = Pattern.compile("^\\s*(create|drop|select|update|delete|insert)\\s+");
		Matcher mat = pat.matcher(query);
		boolean success = false;
		if(mat.find()){
			String cur = mat.group().trim();
			if(cur.equals("create")){
				success = database.executeStructureQuery(query);
				if(success)
					canStart = true;
			}else if(cur.equals("drop")){
				success = database.executeStructureQuery(query);
				if(success)
					canStart = false;
			}
			else if((cur.equals("update") || cur.equals("delete") || cur.equals("insert")) && canStart){
				int returnedValue = database.executeUpdateQuery(query);
				if(returnedValue != 0)
					success = true;
			}else if(cur.equals("select") && canStart){
				Object[][] returnedValue = database.executeQuery(query);
				if(returnedValue.length != 0)
					success = true;
			}else
				throw new RuntimeErrorException(null);
		}
		return success;
	}
	@Override
	public int getQueryTimeout() throws SQLException {

		return 0;
	}

	@Override
	public void close() throws SQLException {

		connection = null;
		batch = null;
		database = null;
	}

	@Override
	public void setQueryTimeout(int seconds) throws SQLException {
		
	}
}
