package eg.edu.alexu.csd.oop.jdbc;

import java.sql.SQLException;
import java.sql.Statement;

import eg.edu.alexu.csd.oop.db.DatabaseEngine;

public class ConnectionAdapterEngine extends ConnectionEngine {
	
		private StatementEngine statement;
		private String databaseDir;
		private DatabaseEngine database;

		public ConnectionAdapterEngine(String path){
			databaseDir = path;
			database = new DatabaseEngine(databaseDir);
		}
		
		
		@Override
		public Statement createStatement() throws SQLException {
			statement = new StatementAdapterEngine(this, database);
			return statement;
		}

		
		@Override
		public void close() throws SQLException {
			statement = null;
			database = null;
			databaseDir = null;
		}
		
		
}
