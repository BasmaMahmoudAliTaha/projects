package eg.edu.alexu.csd.oop.jdbc;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

public class ResultSetMetaDataAdabterEngine extends ResultSetMetaDataEngine{
	private LinkedHashMap<String, Object> property;
	private String tableName;
	private Object[][] data;

	public ResultSetMetaDataAdabterEngine(Object[][] data,
			LinkedHashMap<String, Object> property, String tableName) {
		this.data = data;
		this.property = property;
		this.tableName = tableName;
	}

	@Override
	public int getColumnCount() throws SQLException {
		// TODO Auto-generated method stub
		return data[0].length;
	}

	@Override
	public String getColumnLabel(int column) throws SQLException {
		// TODO Auto-generated method stub
		return getColumnName(column);
	}

	@Override
	public String getColumnName(int column) throws SQLException {
		// TODO Auto-generated method stub
		if (column > property.size())
			throw new SQLException();
		else {
			int i = 0;
			for (String key : property.keySet()) {
				i++;
				if (i == column)
					return key;
			}
			throw new SQLException();
		}
	}

	@Override
	public int getColumnType(int column) throws SQLException {
		if (column > property.size())
			throw new SQLException();
		else {
			int i = 0;
			for (Map.Entry<String, Object> entry : property.entrySet()) {
				i++;
				if (i == column) {
					if (entry.getValue().equals("null"))
						return java.sql.Types.VARCHAR;
					else if (entry.getValue().equals("max"))
						return java.sql.Types.INTEGER;
				}
			}
			throw new SQLException();
		}
	}
	@Override
	public String getTableName(int column) throws SQLException {
		return tableName;
	}
}
