package eg.edu.alexu.csd.oop.jdbc;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Properties;

public class DriverAdapterEngine  extends DriverEngine{

	
	private String path;

	@Override
	public boolean acceptsURL(String arg0) throws SQLException {
		return true;
	}

	@Override
	public Connection connect(String arg0, Properties arg1) throws SQLException {

		File path1 =  (File) arg1.get("path");
		path = path1.getAbsolutePath().toString();
		return new ConnectionAdapterEngine(path);
	}

	@Override
	public DriverPropertyInfo[] getPropertyInfo(String arg0, Properties arg1)
			throws SQLException {

		DriverPropertyInfo info[] = new DriverPropertyInfo[arg1.keySet().size()];
		Iterator<Object> itr = arg1.keySet().iterator();
		int counter = 0;
		while (itr.hasNext()) {
			String str = (String) itr.next();
			info[counter++] = new DriverPropertyInfo(str, arg1.getProperty(str));
		}
		return info;
	}

}