package eg.edu.alexu.csd.oop.jdbc;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Set;

public class ResultSetAdabterEngine extends ResultSetEngine{
	private Object[][] data;
	private int pointer = -1;
	private LinkedHashMap<String, Object> property;
	private String tableName;
	private Statement statement;

	public ResultSetAdabterEngine(Object[][] data, LinkedHashMap<String, Object> tableProperty,
			String tableName, Statement statCurrentObject) {
		this.data = data;
		this.property = tableProperty;
		this.tableName = tableName;
		this.statement = statCurrentObject;
	}
	@Override
	public boolean absolute(int row) throws SQLException {
		if(data.length!=0){
			if (Math.abs(row) <= data.length) {
				if ((row > 0)) { // +ve
					pointer = row--;
					return true;
				} else if ((row < 0)) { // -ve
					pointer = data.length + row;
					return true;
				} else {
					// row = zero
					pointer = -1;
					return false;
				}

			} else { // greater than the length or beyond the first row.
				if (row < (-1 * data.length)) {
					pointer = -1;
				} else {
					pointer = data.length;
				}
				return false;
			}
			// when we use exceptions here?!!
		}
		else 
			return false;
	}

	// //////////////////////////////////////////////////////////////////////////////////
	@Override
	public void afterLast() throws SQLException {
		if(data.length!=0)
			pointer = data.length;
	}

	// ///////////////////////////////////////////////////////////////////////////////////
	@Override
	public void beforeFirst() throws SQLException {
		if(data.length!=0)
			pointer = -1;
	}
	// ///////////////////////////////////////////////////////////////////////////////////////
	@Override
	public void close() throws SQLException {
		// TODO Auto-generated method stub
		pointer = -1;
		throw new SQLException();
	}

	// //////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public int findColumn(String columnLabel) throws SQLException {
		// no handling here for empty data
		Set<String> columnNames = property.keySet();
		int i = 0;
		for (String key : columnNames) {
			i++;
			if (key.equals(columnLabel.toLowerCase()))
				return i;
		}
		// throw sql exception when not finding the colName ??????????????
		throw new SQLException();
	}

	// ////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean first() throws SQLException {
		return absolute(1);
	}
	// /////////////////////////////////////////////////////////////////////////////////
	@Override
	public int getInt(int columnIndex) throws SQLException {
		if(data.length == 0)
			return 0;
		else{
			columnIndex--;
			try {
				if (data[pointer][columnIndex] == "")
					return 0;
				else if (data[pointer][columnIndex] instanceof Integer) {
					return (int) data[pointer][columnIndex];
				} else
					throw new SQLException();
			} catch (Exception e) {
				throw new SQLException();
			}
		}

	}
	// //////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		ResultSetMetaDataEngine rsmd = new ResultSetMetaDataAdabterEngine(data,property, tableName);
		return rsmd;
	}
	// //////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public Object getObject(int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		if(data.length==0)
			return 0;
		else{
			columnIndex--;
			try {
				return data[pointer][columnIndex];
			} catch (Exception e) {
				throw new SQLException();
			}
		}

	}

	// //////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public Statement getStatement() throws SQLException {
		// TODO Auto-generated method stub
		return statement;
	}

	// /////////////////////////////////////////////////////////////*/////////////
	@Override
	public String getString(int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		if(data.length == 0){
			return null;
		}
		else{
			columnIndex--;
			try {
				if (data[pointer][columnIndex] == "")
					return null;
				else if (data[pointer][columnIndex] instanceof String) {
					return (String) data[pointer][columnIndex];
				} else
					throw new SQLException();
			} catch (Exception e) {
				throw new SQLException();
			}
		}
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getString(String columnLabel) throws SQLException {
		// TODO Auto-generated method stub
		return getString(findColumn(columnLabel));
	}
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean isAfterLast() throws SQLException {
		if (data.length == 0)
			return false;
		if (pointer == data.length) {
			return true;
		}
		return false;
	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean isBeforeFirst() throws SQLException {
		// TODO Auto-generated method stub
		if (data.length == 0)
			return false;
		if (pointer == -1) {
			return true;
		}
		return false;
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean isClosed() throws SQLException {
		return true;
	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean isFirst() throws SQLException {
		if (pointer == 0)
			return true;
		return false;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean isLast() throws SQLException {
		// TODO Auto-generated method stub
		if (pointer == (data.length - 1))
			return true;
		return false;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean last() throws SQLException {
		return absolute(-1);
	}
	// //////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean next() throws SQLException {
		// TODO Auto-generated method stub
		if(data.length == 0)
			return false;
		else{
			pointer++;
			if (isAfterLast()) {
				pointer=data.length;
				return false;

			} else
				return true;
		}
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean previous() throws SQLException {
		if(data.length == 0)
			return false;
		else{
			pointer--;
			if(isBeforeFirst()){
				pointer=-1;
				return false;
			}
			else
				return true;
		}
	}
}
