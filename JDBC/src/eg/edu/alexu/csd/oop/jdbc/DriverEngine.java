package eg.edu.alexu.csd.oop.jdbc;

import java.sql.Connection;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

public  abstract class DriverEngine implements java.sql.Driver {

	@Override
	public int getMajorVersion() {
		throw new java.lang.UnsupportedOperationException() ;
	}

	@Override
	public int getMinorVersion() {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean jdbcCompliant() {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public abstract Connection connect(String url, Properties info) throws SQLException;;

	@Override
	public abstract boolean acceptsURL(String url) throws SQLException ;

	@Override
	public abstract DriverPropertyInfo[] getPropertyInfo(String url, Properties info)
			throws SQLException ;
}
