package eg.edu.alexu.csd.oop.calculator;
import java.awt.image.RasterFormatException;

public class SinglyLinkedList implements MyLinkedList {
	private SingleNode head; // head node of the list
	private int size; // number of nodes in the list
	/** Default constructor that creates an empty list */
	public SinglyLinkedList() {
		head = new SingleNode(null , null);
		size = 0;
	}
	@Override
	public void add(int index, Object element) {
		// TODO Auto-generated method stub
		int counter = 1;
		SingleNode y, temp,addedNode;
		y = head.getNext();
		temp = y;
		addedNode = y;
		if( y==null && index!=1) {
			throw new RuntimeException("The list is empty, you have no choice to add except in index 1");
		}
		else if(y==null && index==1){
			y = new SingleNode(element , null);
			size++;
			return;
		}
		if(index < 1 || index > size){ //out of boundaries
			throw new RuntimeException();
		}
		while(counter!=index){
			temp = y; //prev node equals the current pointer node
			y = y.getNext();
			counter++;
		}
			addedNode = new SingleNode(element, y);
			temp.setNext(addedNode);
			size++;
	}
	@Override
	public void add(Object element) {
		// TODO Auto-generated method stub
		SingleNode y, q;
		y = new SingleNode(null, null);
		y = head.getNext();
		q = new SingleNode(null , null);
		if(y == null){ //if list is empty
			y = new SingleNode(element, null);
			head.setNext(y);//set next
			size++;
			return;
		}
		while(y!=null){
			q = y; //prev node
			y = y.getNext();
		}
		y = new SingleNode(element, null);
		q.setNext(y);
		size++;	
	}

	@Override
	public Object get(int index) {
		// TODO Auto-generated method stub
		SingleNode y;
		int counter = 1;
		Object target = null;
		y = head.getNext();
		if(index < 1||index > size){
			throw new RuntimeException(); //out of array boundaries
		}
		if(y.getElement()==null){
			throw new RuntimeException("List is empty");
		}
		while(counter!=index){
			y = y.getNext();
			counter++;
		}
		target = y.getElement();
		return target;
	}

	@Override
	public void set(int index, Object element) {
		// TODO Auto-generated method stub
		int counter = 1;
		SingleNode y;
		y = head.getNext();
		if(index<1||index>size){
			throw new RuntimeException(); //out of array boundaries
		}
		if(y==null) {
			throw new RasterFormatException("List is empty");
		}
		while(counter!=index&&y!=null){
			y = y.getNext();
			counter++;
		}
		if(counter == index){
			y.setElement(element);
		}
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		head.setNext(null);
		size = 0;
		return;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		SingleNode y;
		y = head.getNext();
		if(y == null){
			return true;
		}
		return false;
	}

	@Override
	public void remove(int index) {
		// TODO Auto-generated method stub
		SingleNode y, temp;
		int counter = 1;
		y = head.getNext();
		temp = new SingleNode(null, null);
		if(index < 1||index > size){
			throw new RuntimeException(); //out of array boundaries
		}
		else if(index==1 && size ==1){
			// y.setElement(null);
			head.setNext(null);
			size = 0;
			return;
		}
		else if(index==1 && size == 2){
			y = y.getNext();
			y.setNext(null);
			head.setNext(y);
			size--;
			return;
		}
		else if(index==2 && size == 2){
			
			y.setNext(null);
			head.setNext(y);
			size--;
			return;
		}
		else if(index==1){
			y = y.getNext();
			head.setNext(y);
			/*y.setElement(null);
			y.setNext(null);*/
			size--;
			return;
		}
		while(counter!=index){
				temp = y; //prev node
				y = y.getNext();
				counter++;
		}
		y = y.getNext();
		temp.setNext(y);
		/*y.setElement(null);
		y.setNext(null); //removing the element*/
		size--;
		return;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public MyLinkedList sublist(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		if(fromIndex < 1|| toIndex > size){
			throw new RuntimeException();
		}
		int counter = 1;
		SingleNode y;
		MyLinkedList out = new SinglyLinkedList();
		y = head.getNext();
		if(y==null){ //list is empty
			throw new RuntimeException("List is empty");
		}
		if(y.getNext()==null){ //only one element
			if(fromIndex==toIndex) {
				out.add(y.getElement());
				return out;
			}
			else{
				throw new RuntimeException();
			}
		}
		
		while(counter!=fromIndex){
			y = y.getNext();
			counter++;
		}
		for(int i = fromIndex; i <= toIndex; i++){
			out.add(y.getElement());
			y = y.getNext();
		}
		return out;
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		SingleNode y;
		y = head.getNext();
		if(y == null){
			return false;
		}
		while(y!=null){
			if(y.getElement()==o) return true;
			y = y.getNext();
		}
		return false;
	}

}
