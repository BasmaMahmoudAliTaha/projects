package eg.edu.alexu.csd.oop.calculator;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

public class CalculatorUI extends JFrame {
	private static final long serialVersionUID = 1234567L;
	private JPanel contentPane;
	private JButton button_2;
	private JButton button_3;
	private JButton button_4;
	private JButton button_5;
	private JButton button_6;
	private JButton button_9;
	private JButton button_8;
	private JButton button_7;
	private JButton commaButton;
	private JButton button_0;
	private JButton resultButton;
	private JButton btnNext;
	private JButton btnPrevious;
	private JButton btnCurrent;
	private JButton btnSave;
	private JButton btnLoad;
	private JButton addButton;
	private JButton subtractButton;
	private JButton MultiplyButton;
	private JButton divisionButton;
	private JLabel textField = new JLabel("");
	private ButtonsAction buttonCommand = new ButtonsAction();
	private CalculatorEngine in = new CalculatorEngine();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalculatorUI frame = new CalculatorUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	private String input = null;
	private JButton enter;
	private JButton clear;
	private JButton btnOff;

	private class ButtonsAction {
		private String value;
		private void setValue(String elem) {
			// TODO Auto-generated constructor stub
			this.value = elem;
		}
		private void inputActionPerformed() {

			if(value.equals("=")){
				in.input(input);
				String res = in.getResult();
				if(res == null){
					JOptionPane.showMessageDialog(null, "Input syntax error.. \n Division by zero!\n Or you haven't entered any input!");
				}
				else textField.setText(res);
				input = null;
				return;
			}

			else if(value.equals("Load")) {
				in.load();
				if(!in.isFileEmpty()){
					JOptionPane.showMessageDialog(null, "History loaded Successfuly!");
				}
				else{
					JOptionPane.showMessageDialog(null,"There is no history to be loaded!");
				}
			}

			else if(value.equals("Save")) {
				in.save();
				JOptionPane.showMessageDialog(null, "History saved Successfuly!");
			}
			else if(value.equals("Current")){
				String s = (String)in.current();
				if(s==null) JOptionPane.showMessageDialog(null, "You have no history stored!");
				textField.setText(s);	
			}

			else if(value.equals("Next")){
				String s = (String)in.next();
				if(s==null) 
					JOptionPane.showMessageDialog(null, "Out of history bounds!\n You can Only Save \"5\" Steps");
				else 
					textField.setText(s);
			}

			else if(value.equals("Previous")){
				String s = (String)in.prev();
				if(s==null) 
					JOptionPane.showMessageDialog(null, "Out of history bounds!\n You can Only Save \"5\" Steps");
				else 
					textField.setText(s);
			}

			else{

				if(input==null){
					input = value;
				}		
				else{
					input = input.concat(value);
				}
				textField.setText(input);
			}
		}
	}
	/**
	 * Create the frame.
	 */
	public CalculatorUI() {
		setTitle("Calculator");
		setBackground(Color.DARK_GRAY);

		textField.setForeground(UIManager.getColor("CheckBoxMenuItem.acceleratorForeground"));
		textField.setBackground(Color.lightGray);
		textField.setOpaque(true);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("CheckBoxMenuItem.acceleratorForeground"));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton buttton_1 = new JButton("1");
		buttton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("1");
				buttonCommand.inputActionPerformed();
			}
		});
		buttton_1.setBounds(26, 58, 47, 34);
		contentPane.add(buttton_1);

		button_2 = new JButton("2");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("2");
				buttonCommand.inputActionPerformed();
			}
		});
		button_2.setBounds(85, 58, 47, 34);
		contentPane.add(button_2);

		button_3 = new JButton("3");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("3");
				buttonCommand.inputActionPerformed();
			}
		});
		button_3.setBounds(144, 58, 47, 34);
		contentPane.add(button_3);

		button_4 = new JButton("4");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("4");
				buttonCommand.inputActionPerformed();
			}
		});
		button_4.setBounds(26, 104, 47, 34);
		contentPane.add(button_4);

		button_5 = new JButton("5");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("5");
				buttonCommand.inputActionPerformed();
			}
		});
		button_5.setBounds(85, 104, 47, 34);
		contentPane.add(button_5);

		button_6 = new JButton("6");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("6");
				buttonCommand.inputActionPerformed();
			}
		});

		button_6.setBounds(144, 104, 47, 34);
		contentPane.add(button_6);

		button_9 = new JButton("9");
		button_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("9");
				buttonCommand.inputActionPerformed();
			}
		});
		button_9.setBounds(144, 153, 47, 34);
		contentPane.add(button_9);

		button_8 = new JButton("8");
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("8");
				buttonCommand.inputActionPerformed();
			}
		});
		button_8.setBounds(85, 150, 47, 34);
		contentPane.add(button_8);

		button_7 = new JButton("7");
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("7");
				buttonCommand.inputActionPerformed();
			}
		});
		button_7.setBounds(26, 150, 47, 34);
		contentPane.add(button_7);

		commaButton = new JButton(".");
		commaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue(".");
				buttonCommand.inputActionPerformed();
			}
		});
		commaButton.setBounds(26, 196, 47, 34);
		contentPane.add(commaButton);

		button_0 = new JButton("0");
		button_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("0");
				buttonCommand.inputActionPerformed();
			}
		});
		button_0.setBounds(85, 196, 47, 34);
		contentPane.add(button_0);

		resultButton = new JButton("=");
		resultButton.setToolTipText("");
		resultButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("=");
				buttonCommand.inputActionPerformed();
			}
		});
		resultButton.setBounds(262, 180, 68, 50);
		contentPane.add(resultButton);

		btnNext = new JButton("Next");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("Next");
				buttonCommand.inputActionPerformed();
			}
		});
		btnNext.setBounds(342, 58, 96, 34);
		contentPane.add(btnNext);

		btnPrevious = new JButton("Previous");
		btnPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("Previous");
				buttonCommand.inputActionPerformed();
			}
		});
		btnPrevious.setBounds(342, 104, 96, 34);
		contentPane.add(btnPrevious);

		btnCurrent = new JButton("Current");
		btnCurrent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("Current");
				buttonCommand.inputActionPerformed();
			}
		});
		btnCurrent.setBounds(342, 150, 96, 34);
		contentPane.add(btnCurrent);

		btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("Save");
				buttonCommand.inputActionPerformed();
			}
		});
		btnSave.setBounds(342, 196, 96, 34);
		contentPane.add(btnSave);

		btnLoad = new JButton("Load");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("Load");
				buttonCommand.inputActionPerformed();
			}
		});
		btnLoad.setBounds(342, 11, 96, 34);
		contentPane.add(btnLoad);

		addButton = new JButton("+");
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("+");
				buttonCommand.inputActionPerformed();
			}
		});
		addButton.setBounds(203, 58, 47, 34);
		contentPane.add(addButton);

		subtractButton = new JButton("-");
		subtractButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("-");
				buttonCommand.inputActionPerformed();
			}
		});
		subtractButton.setBounds(203, 104, 47, 34);
		contentPane.add(subtractButton);

		MultiplyButton = new JButton("*");
		MultiplyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("*");
				buttonCommand.inputActionPerformed();
			}
		});
		MultiplyButton.setBounds(203, 153, 47, 34);
		contentPane.add(MultiplyButton);

		divisionButton = new JButton("/");
		divisionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.setValue("/");
				buttonCommand.inputActionPerformed();
			}
		});
		divisionButton.setBounds(203, 196, 47, 34);
		contentPane.add(divisionButton);


		textField.setBackground(UIManager.getColor("Button.light"));
		textField.setBounds(26, 12, 224, 34);
		contentPane.add(textField);

		enter = new JButton("AC");
		enter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(input == null)
					textField.setText(null);

				else{
					textField.setText(null);
					in.input(input);
					input = null;
				}
			}
		});
		enter.setBounds(262, 58, 68, 45);
		contentPane.add(enter);

		clear = new JButton("C");
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				StringBuilder sb = new StringBuilder(input);
				sb.deleteCharAt(input.length()-1);
				input = sb.toString();
				textField.setText(input);
			}
		});
		clear.setBounds(262, 115, 68, 53);
		contentPane.add(clear);

		btnOff = new JButton("Off");
		btnOff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnOff.setBounds(144, 196, 57, 34);
		contentPane.add(btnOff);

	}
}
