package eg.edu.alexu.csd.oop.calculator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Singleton design pattern applied
public class Operations {
	private double input1, input2, result;
	private static Operations firstInstance = null;

	private Operations(){}

	public static Operations getInstance(){
		if(firstInstance == null){
			firstInstance = new Operations();
		}
		return firstInstance;
	}
	public String getResult (String input){
		Pattern p = Pattern.compile("\\s*([0-9]*(\\.{1}[0-9]*)?)\\s*(\\/||\\+||\\-||\\*)\\s*([0-9]*(\\.{1}[0-9]*)?)\\s*");
		Matcher m = p.matcher(input);
		if(m.matches()){
			input1 =  Double.parseDouble(m.group(1));
			input2 =  Double.parseDouble(m.group(4));

			if(m.group(3).equals("-"))
				subtract();
			else if(m.group(3).equals("*"))
				multiply();
			else if(m.group(3).equals("+"))
				add();
			else if(m.group(3).equals("/"))
				divide();
			else 
				throw new RuntimeException("Input syntax error");
		}	
		else 
			throw new RuntimeException("Input syntax error");

		return String.valueOf(result);
	}
	private void add(){
		result = input1 + input2;
	}

	private void divide(){ 
		if(input2 == 0) 
			throw new RuntimeException("Division By Zero");
		else {
			result = input1 / input2;
			System.out.println(result);
		}

	}

	private void multiply(){
		result = input1 * input2;
	}

	private void subtract(){
		result = input1 - input2;
	}
}
