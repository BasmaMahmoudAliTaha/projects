package eg.edu.alexu.csd.oop.calculator;

public class SingleNode {
	private Object element; // we assume elements are character strings
	private SingleNode next;
	/** Creates a node with the given element and next node. */
	public SingleNode(Object s, SingleNode n) {
	element = s; next = n;
	}
	/** Returns the element of this node. */
	public Object getElement() { return element; }
	/** Returns the next node of this node. */
	public SingleNode getNext() { return next; }
	// Modifier methods:
	/** Sets the element of this node. */
	public void setElement(Object newElem) { element = newElem; }
	/** Sets the next node of this node. */
	public void setNext(SingleNode newNext) { next = newNext; }
}
