package eg.edu.alexu.csd.oop.calculator;

public class History {
	private SinglyLinkedList list = new SinglyLinkedList();
	private int historySize = 0;
	private int prev = 0, next = 0;
	private int current = -1; 
	private int preLastCurrent = 0;
	private boolean donePrevOrNextOp = false;
	/*private static History firstInstance = null;

	private History(){}
	
	public static History getInstance(){
		if(firstInstance == null){
			firstInstance = new History();
		}
		return firstInstance;
	}*/
	
	public boolean getOperationHistory(){
		return donePrevOrNextOp;
	}
	
	public void initializeVars(){
		prev = 0;
		next = 0;
		current = -1;
		preLastCurrent = 0;
		donePrevOrNextOp = false;
		historySize = 0 ;
		list.clear();
	}
	
	public void addHistory(String in){
		list.add((String)in);
		historySize = list.size();
		
		if(historySize > 5) {
			list.remove(1);
			historySize--;
		}
		if(prev!=0||next!=0){
			donePrevOrNextOp = true;// to maintain the current as its last value even when there has been history op done
			preLastCurrent = current;
		}
		current = list.size();
	}
	public int current(){
		if(current == -1) return -1; //error happened (no history)
		return current;
	}
	public int returnPreLastCurrent(){
		return preLastCurrent;
	}
	public String getCurrent(){
		if(current == -1) return null;
		else{
			if(donePrevOrNextOp)
				return (String) list.get(preLastCurrent+1);
			else
				return (String) list.get(current);
		}	
	}

	public SinglyLinkedList returnHistory(){
		return list;
	}
	
	public String getPrev(){//int prevCounter){		
		try {
			//prev = list.size() - prevCounter;
			prev = current - 1;
			current = prev;
			return (String)list.get(prev);
			
		}catch(RuntimeException e){
			donePrevOrNextOp = false;
			if(list.isEmpty())
				current = -1;
			else{
				current = 1;
			}
		}
		
		if(list.isEmpty())
			current = -1;
		else {
			current = 1;	
		}
		return null; // error
	}
	
	public String getNxt(){
		try{
			next = current + 1;
			current = next;
			return (String) list.get(next);
			
		}catch(RuntimeException e){
			donePrevOrNextOp = false;
			if(list.size() == 1){
				current = 1;
			}

			else if(next > list.size()){
				current = list.size();
			}
			else if(list.isEmpty())
				current = -1;
		}	
		return null; // error
	}
}
