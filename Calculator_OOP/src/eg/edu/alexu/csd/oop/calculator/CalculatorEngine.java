package eg.edu.alexu.csd.oop.calculator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class CalculatorEngine implements Calculator {
	private String desiredResult;
	private SinglyLinkedList list;
	private boolean fileIsEmpty = true;
	private boolean noInput = true;
	private History history = new History();
	private Operations op = Operations.getInstance();
	@Override
	public void input(String s) {
		if(s!=null) 
			history.addHistory(s);
		list = history.returnHistory();
		noInput = false;
	}

	@Override
	public String getResult() {
		if(list.isEmpty()) return null;
		else{
			desiredResult = (String)list.get(history.current());

			if(history.getOperationHistory()){ // deleting the history after the current
				int n = history.returnPreLastCurrent();
				for(int i = list.size()-1; i > n; i--){
					list.remove(i);
				}
			}
			try{
				desiredResult = op.getResult(desiredResult);
			}
			catch(RuntimeException ex){
				desiredResult = null; // there is an error happened
				throw new RuntimeException();
			}
		}
		return desiredResult;
	}

	@Override
	public String current() {
		desiredResult = history.getCurrent();
		if(desiredResult == null) 
			return null; ///error(no history)
		return desiredResult;
	}

	@Override
	public String prev() {
		desiredResult = history.getPrev();
		return desiredResult;
	}

	@Override
	public String next() {
		desiredResult = history.getNxt();
		return desiredResult;
	}

	@Override
	public void save() {
		PrintWriter writer;
		try {
			writer = new PrintWriter("saved history.txt", "UTF-8");
			for(int i = 1; i <= history.current() ; i++){
				writer.println((String)list.get(i));
			}
			writer.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void load() {

		if(!noInput && !list.isEmpty()){
			list.clear();
			noInput = true;
			history.initializeVars();
		}
		try{
			FileReader inputFile = new FileReader("saved history.txt");
			BufferedReader bufferReader = new BufferedReader(inputFile);

			String line;

			while ((line = bufferReader.readLine()) != null)  {
				fileIsEmpty = false;
				history.addHistory(line);
			}
			//Close the buffer reader
			bufferReader.close();
			if(fileIsEmpty) list = null;
			else 
				list = history.returnHistory();
		}catch(IOException e){
			throw new RuntimeException();
		}	
	}

	public boolean isFileEmpty(){
		return fileIsEmpty;
	}

}
