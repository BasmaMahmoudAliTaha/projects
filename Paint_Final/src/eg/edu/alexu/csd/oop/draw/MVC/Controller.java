package eg.edu.alexu.csd.oop.draw.MVC;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import eg.edu.alexu.csd.oop.draw.DrawEngine;
import eg.edu.alexu.csd.oop.draw.Shape;


public class Controller {
	private DrawEngine drawModel;
	private View view;
	private Canvas canvas;
	private Shape selectedShape;
	private String currentShape;
	private Point begin , end;
	private Color stroke = Color.BLACK;
	private Color fill = Color.WHITE;
	private static Controller firstInstance = null;
	
	private Controller(DrawEngine drawModel, View view){
		this.drawModel = drawModel;
		this.view = view;
		this.view.setSupportedShapes(drawModel.getSupportedShapes());
		this.view.initialize();
		canvas = view.getCanvas();

		MouseMotion mouse = new MouseMotion();		
		this.view.addMouseMotionListener(mouse);

		this.view.addBtnNameListener(new BtnNameListener());

		this.view.addSaveBtnListener(new SaveBtnListener());
		this.view.addLoadBtnListener(new LoadBtnListener());
		this.view.addUndoBtnListener(new UndoBtnListener());
		this.view.addRedoBtnListener(new RedoBtnListener());
		this.view.addStrokeColBtnListener(new StrockBtnListener());
		this.view.addFillColBtnListener(new FillBtnListener());

	}
	public static Controller getInstance(DrawEngine d, View v){
		if(firstInstance == null){
			firstInstance = new Controller (d, v);
		}
		return firstInstance;
	}

	private JFileChooser addFileChooser(){
		JFileChooser fileChooser = new JFileChooser();
		FileFilter xmlFilter = new FileTypeFilter(".xml", "XML files");
		FileFilter jsonFilter = new FileTypeFilter(".json", "JSON Files");

		fileChooser.addChoosableFileFilter(xmlFilter);
		fileChooser.addChoosableFileFilter(jsonFilter);
		
		return fileChooser;
	}
	
	
	private class BtnNameListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			currentShape = e.getActionCommand();
		}

	}
	
	private class SaveBtnListener implements ActionListener{
		private JFileChooser fileChooser;
		private SaveBtnListener() {
			this.fileChooser = addFileChooser();
		}

		@Override
		public void actionPerformed(ActionEvent e) {

			fileChooser.setDialogTitle("Save as:");

			int userSelection = fileChooser.showSaveDialog(null);

			if (userSelection == JFileChooser.APPROVE_OPTION) {
				try{
					drawModel.save(fileChooser.getSelectedFile().toString());
				}catch(RuntimeException ex){
					View.displayErrorMessage("Cannot save file");
				}
			}
		}

	}
	
	private class LoadBtnListener implements ActionListener{
		private JFileChooser fileChooser;
		private LoadBtnListener() {
			this.fileChooser = addFileChooser();
		}

		@Override
		public void actionPerformed(ActionEvent e) {

			fileChooser.setDialogTitle("Load file:");
			int userSelection = fileChooser.showOpenDialog(null);
			if (userSelection == JFileChooser.APPROVE_OPTION) {

				File selectedFile = fileChooser.getSelectedFile();
				try{
					drawModel.load(selectedFile.getAbsolutePath());
					canvas.paint(canvas.getGraphics());
					drawModel.refresh(canvas.getGraphics());
				}catch(RuntimeException ex){
					View.displayErrorMessage("Cannot Load File");
				}
			}

		}

	}
	
	private class UndoBtnListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			try{
				drawModel.undo();
			}catch(RuntimeException ex){
				View.displayErrorMessage("Cannot undo");
			}
			canvas.paint(canvas.getGraphics());
			drawModel.refresh(canvas.getGraphics());
		}
		
	}
	
	private class RedoBtnListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			try{
				drawModel.redo();
			}catch(RuntimeException ex){
				View.displayErrorMessage("Cannot redo");
			}			
			canvas.paint(canvas.getGraphics());
			drawModel.refresh(canvas.getGraphics());
		}
		
	}
	
	private class StrockBtnListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			stroke = JColorChooser.showDialog(null, "Choose stroke Color", Color.BLACK);
		}
		
	}
	
	private class FillBtnListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			fill = JColorChooser.showDialog(null, "Choose Fill Color", Color.WHITE);
		}
		
	}
	
	private class MouseMotion extends MouseAdapter{

		private Shape checkPosition(Point e){
			Shape [] shapes = drawModel.getShapes(); // Controller	sends arr of shapes
			Shape selectShape = null;
			Point minDistance ;

			for(Shape s : shapes){
				Map<String , Double> mapShape = s.getProperties();
				Iterator<String> itr = mapShape.keySet().iterator();
				Point onScreen = e;
				Point position = s.getPosition();
				int min = 0;
				while(itr.hasNext() ){
					String temp = itr.next();
					if(temp.equals("X-axis"))
						min = Math.max(min , Math.abs((int)mapShape.get(temp).doubleValue() - position.x) );
					else if(temp.equals("Y-axis"))
						min = Math.max(min , Math.abs((int)mapShape.get(temp).doubleValue() - position.y) );
					else
						min = Math.max((int)mapShape.get(temp).doubleValue(), min);
				}
				minDistance = new Point(min , min);
				if(( onScreen.x - position.x  <= minDistance.x) && (onScreen.y - position.y  <= minDistance.y) 
						&& (onScreen.x - position.x  > 0) && ( onScreen.y - position.y  > 0) ){
					minDistance.x = onScreen.x - position.x ;
					minDistance.y = onScreen.y - position.y  ;
					selectShape = s;
				}else if(s.getClass().getSimpleName().equals("Line") && ((Math.abs( onScreen.x - position.x)  <= minDistance.x) && (Math.abs(onScreen.y - position.y)  <= minDistance.y) 
						&& (Math.abs(onScreen.x - position.x)  > 0) && ( Math.abs(onScreen.y - position.y)  > 0))  ){
					minDistance.x = onScreen.x - position.x ;
					minDistance.y = onScreen.y - position.y  ;
					selectShape = s;
				}

			}
			return selectShape;
		}
		@Override
		public void mouseClicked(MouseEvent e){
			if(currentShape.equals("delete") || currentShape.equals("changeColor")){
				selectedShape = checkPosition(e.getPoint());
			}
			if(selectedShape != null && currentShape.equals("delete")){
				drawModel.removeShape(selectedShape); //remove from controller
				canvas.paint(canvas.getGraphics());
				drawModel.refresh(canvas.getGraphics());
			}else if(selectedShape != null && currentShape.equals("changeColor")){
				try {
					Shape newShape = (Shape) selectedShape.clone();
					newShape.setColor(JColorChooser.showDialog(null, "Change Stroke Color", Color.BLACK));
					newShape.setFillColor(JColorChooser.showDialog(null, "Change fill Color", Color.RED));
					drawModel.updateShape(selectedShape, newShape);
					canvas.paint(canvas.getGraphics());
					drawModel.refresh(canvas.getGraphics());
					selectedShape = null;
					
				} catch (CloneNotSupportedException e1) {
					e1.printStackTrace();
				}				

			}
		}


		@Override
		public void mousePressed(MouseEvent e) {
			// When the mouse is pressed get x & y position
			if(currentShape.equals("move") || currentShape.equals("resize"))
				selectedShape = checkPosition(e.getPoint());
			begin = new Point(e.getX(), e.getY());
			end = begin;
		}

		@Override
		public void mouseReleased(MouseEvent e) {

			if(currentShape.equals("move") ){
				if(selectedShape != null){
					try{
						Shape newShape = (Shape) selectedShape.clone();
						if(newShape.getClass().getSimpleName().equals("Line")){
							Point newP = newShape.getPosition();
							Map<String , Double> map1 = newShape.getProperties();
							Point newPoint2 = new Point(newP.x  - e.getX() ,newP.y  - e.getY() ) ;
							map1.put("X-axis", map1.get("X-axis") - newPoint2.getX());
							map1.put("Y-axis", map1.get("Y-axis") - newPoint2.getY());
							newShape.setPosition(e.getPoint());
							newShape.setProperties(map1);
						}else{
							newShape.setPosition(e.getPoint());
						}
						drawModel.updateShape(selectedShape, newShape);
						canvas.paint(canvas.getGraphics());
						drawModel.refresh(canvas.getGraphics());
						selectedShape = null;
					}catch(CloneNotSupportedException ex){
						ex.printStackTrace();
					}
				}
			}else if(currentShape.equals("resize") ){
				if(selectedShape != null){
					try{
						begin = selectedShape.getPosition();
						end = e.getPoint();
						Shape newShape = (Shape) selectedShape.clone();
						Map<String , Double> map = newShape.getProperties();
						Iterator<String> itr = map.keySet().iterator();
						while(itr.hasNext()){
							String str1 = itr.next();
							if(str1.equals("Width") || str1.equals("Major Axe") || str1.equals("Length") || str1.equals("Diameter")){
								map.put(str1, Double.parseDouble((""+(Math.abs( begin.x - end.x )))));
							}
							else if(str1.equals("Height") || str1.equals("Minor Axe") || str1.equals("Side of Triangle")){
								map.put(str1, Double.parseDouble((""+(Math.abs(begin.y - end.y )))));
							}
							else if(str1.equals("X-axis")){
								map.put(str1 , Double.parseDouble((""+end.x)));
							}
							else if(str1.equals("Y-axis")){
								map.put(str1 , Double.parseDouble((""+end.y)));
							}else
							{
								boolean flag = false;
								while(!flag){
									try{
										map.put(str1, Double.parseDouble(JOptionPane.showInputDialog(null, "Enter "+str1 )));
										flag = true;
									}catch(NumberFormatException ex){
										View.displayErrorMessage("Wrong entry");
									}
								}
							}
						}
						newShape.setProperties(map);
						drawModel.updateShape(selectedShape, newShape);
						canvas.paint(canvas.getGraphics());
						drawModel.refresh(canvas.getGraphics());
						selectedShape = null;
					}catch(CloneNotSupportedException ex){
						ex.printStackTrace();
					}
				}
			}else if(!currentShape.equals("delete") && !currentShape.equals("changeColor")){	
				end = new Point(e.getX(), e.getY());
				RecordShape a = new RecordShape(currentShape, begin, end, stroke, fill);
				a.setKeysAndPos();
				a.fillMapProperties();
				drawModel.addShape(a.setShape());
				canvas.paint(canvas.getGraphics());
				drawModel.refresh(canvas.getGraphics());
			}
			begin = null;
			end = null;
		}

		
	}

}
