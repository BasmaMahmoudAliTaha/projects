package eg.edu.alexu.csd.oop.draw.SaveAndLoad;

import java.awt.Color;
import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import eg.edu.alexu.csd.oop.draw.Command;
import eg.edu.alexu.csd.oop.draw.Factory;
import eg.edu.alexu.csd.oop.draw.Shape;

public class LoadJsonCommand implements Command {
	
	private String fileNameAndDirec;
	private  List<Shape>  loadedList ;

	public LoadJsonCommand (String fileNameAndDirec) {
		this.fileNameAndDirec = fileNameAndDirec;		
	}
	@Override
	public void execute() {
		readFromJson();
	}
	
	
	public List<Shape> getLoadedList(){
		return loadedList;
	}
	
	private void readFromJson(){
		loadedList = new ArrayList<Shape>(); 
		File file = new File(fileNameAndDirec);
		Scanner sc;

		try{
			sc = new Scanner(file);
		}catch(FileNotFoundException e){
			throw new RuntimeException("File not found!");
		}

		sc.nextLine();
		sc.nextLine();
		sc.nextLine();
		while (!sc.hasNext("]")) {
			Shape s = readShapes(sc);
			if(s!=null)
				loadedList.add(s);
		}
		sc.close();
	}

	private Shape readShapes (Scanner sc){
		String classPath = sc.nextLine();
		classPath = classPath.replaceAll("\\s+", "");
		classPath = classPath.replaceAll("\\{\"className\":\"", "");
		classPath = classPath.replaceAll("\",", "");

		Shape shape;
		try {
			shape = Factory.getInstance().getShape(classPath);
			Map<String, Double> m = shape.getProperties();

			// read and set stroke color
			String color = sc.nextLine();
			color  =  returnProperty(color);

			if(!color.equals("null")){
				shape.setColor(new Color(Integer.parseInt(color)));
			}

			// read and set fill color
			String fillColor = sc.nextLine();
			fillColor  =  returnProperty(fillColor);
			if(!fillColor.equals("null")){
				shape.setFillColor(new Color(Integer.parseInt(fillColor)));
			}

			// read and set position
			String posX = sc.nextLine();
			posX  =  returnProperty(posX);

			String posY = sc.nextLine();
			posY  =  returnProperty(posY);

			if(!(posX.equals("null")||posY.equals("null"))){
				int x = Integer.parseInt(posX);
				int y = Integer.parseInt(posY);
				Point p = new Point(x,y);
				shape.setPosition(p);
			}
			// fill map with written properties
			String str = sc.nextLine();
			str  = returnProperty(str);
			
			if(!str.equals("nullmap")){
				Set<String> keys = m.keySet();
				int counter = 0;
				for(String k : keys){
					if(str.equals("null"));
					else {
						Double val = Double.parseDouble(str);
						m.put(k, val);
					}
					counter++;
					if(counter!=keys.size()){
						str = sc.nextLine();
						str  = returnProperty(str);
					}
				}
			}

			sc.nextLine();
			shape.setProperties(m);
			return shape;
		} catch (ClassNotFoundException | NoSuchMethodException
				| InstantiationException | IllegalAccessException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	private String returnProperty(String s){
		
		s  =  s.replaceAll("\\s+", "");
		s = s.split(":")[1];
		s  =  s.replaceAll("\"+", "");
		s  =  s.replaceAll("\"+", "");
		s  =  s.replaceAll(",", "");
		
		return s;
	}
}
