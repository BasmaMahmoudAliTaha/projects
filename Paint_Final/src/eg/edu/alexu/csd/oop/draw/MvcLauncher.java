package eg.edu.alexu.csd.oop.draw;

import eg.edu.alexu.csd.oop.draw.MVC.*;

public class MvcLauncher {
	/**
	 * Launch the Application
	 */
	public static void main(String[] args) {

		View theView = View.getInstance();

		DrawEngine theModel = new DrawEngine();
		Controller.getInstance(theModel, theView);
	}
}
