package eg.edu.alexu.csd.oop.draw;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public abstract class ShapeDrawer implements Shape ,  Cloneable  {

	private int x = 0;
	private int y = 0;
	private Map<String , Double> m ;
	private Color fillColor = new Color(0, 0,0, 0);
	private Color color =  new Color(0, 0,0, 0);

	@Override
	public void setPosition(Point position) {
		this.x = position.x;
		this.y = position.y;
	}

	@Override
	public Point getPosition() {
		return new Point(x,y);
	}

	@Override
	public void setProperties(Map<String, Double> properties) {
		m = properties;
	}

	@Override
	public Map<String, Double> getProperties() {
		return m;
	}

	@Override
	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public Color getColor() {
		return this.color;
	}

	@Override
	public void setFillColor(Color color) {
		this.fillColor = color;
	}

	@Override
	public Color getFillColor() {
		return this.fillColor;
	}

	public abstract void draw(Graphics canvas);

	@Override
	public Object clone() throws CloneNotSupportedException {
		Shape gs = null;
		try{
			Class<?> cls = Class.forName(this.getClass().getName());
			Constructor<?> constructor =  cls.getConstructor();
			gs = (Shape) constructor.newInstance();
			gs.setPosition(this.getPosition());
			gs.setColor(this.getColor());
			gs.setFillColor(this.getFillColor());
			gs.setProperties(this.getProperties());

		}catch(ClassNotFoundException | NoSuchMethodException | 
				IllegalAccessException | InvocationTargetException | InstantiationException ex){
			ex.printStackTrace();
		}
		return gs;

	}

}