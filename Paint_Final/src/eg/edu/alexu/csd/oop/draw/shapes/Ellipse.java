package eg.edu.alexu.csd.oop.draw.shapes;


import java.awt.Graphics;
import java.awt.Point;
import java.util.LinkedHashMap;
import java.util.Map;

import eg.edu.alexu.csd.oop.draw.ShapeDrawer;

public class Ellipse extends ShapeDrawer{

	public Ellipse() {
		Map<String , Double> m = new LinkedHashMap<String , Double>();
		m.put("Minor Axe", 0.0);
		m.put("Major Axe", 0.0);
		setProperties(m);
	}


	@Override
	public void draw(Graphics canvas) {
		Map<String , Double> m = getProperties();
		Point p = this.getPosition();
	
		canvas.setColor(this.getColor());
		canvas.drawOval((int) p.getX() , (int) p.getY(), ((int) m.get("Major Axe").doubleValue()), ((int) m.get("Minor Axe").doubleValue()));
		canvas.setColor(this.getFillColor());
		canvas.fillOval((int) p.getX() , (int) p.getY(), ((int) m.get("Major Axe").doubleValue()), ((int) m.get("Minor Axe").doubleValue()));
	}

}
