package eg.edu.alexu.csd.oop.draw.SaveAndLoad;

import java.util.List;

import eg.edu.alexu.csd.oop.draw.Command;
import eg.edu.alexu.csd.oop.draw.Shape;

public class ExecuteSaveAndLoadCommand {
	
	private Command theCommand;
	private  List<Shape>  loadedList;
	
	public ExecuteSaveAndLoadCommand(Command c){
		this.theCommand = c;
	}
	
	public void press(){
		theCommand.execute();
		
		if(theCommand instanceof LoadXmlCommand) {
			this.loadedList = ((LoadXmlCommand) theCommand).getLoadedList();
		}
		else if(theCommand instanceof LoadJsonCommand){
			this.loadedList = ((LoadJsonCommand) theCommand).getLoadedList();
		}
	}
	
	public List<Shape> getLoadedList(){
		return loadedList;
	}
}
