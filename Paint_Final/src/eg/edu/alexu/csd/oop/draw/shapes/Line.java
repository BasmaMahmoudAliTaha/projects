package eg.edu.alexu.csd.oop.draw.shapes;

import java.awt.Graphics;
import java.awt.Point;
import java.util.LinkedHashMap;
import java.util.Map;

import eg.edu.alexu.csd.oop.draw.ShapeDrawer;

public class Line extends ShapeDrawer {

	public Line(){
		Map<String , Double> m = new LinkedHashMap<String, Double>();
		m.put("X-axis", 0.0);
		m.put("Y-axis", 0.0);
		setProperties(m);
	}

	@Override
	public void draw(Graphics canvas) {
		Map<String , Double> m = getProperties();
		Point p = getPosition();
		canvas.setColor(getColor());
		canvas.drawLine((int)p.getX() ,(int)p.getY() , (int)(m.get("X-axis").doubleValue()),  (int)(m.get("Y-axis").doubleValue()));
	}
}
