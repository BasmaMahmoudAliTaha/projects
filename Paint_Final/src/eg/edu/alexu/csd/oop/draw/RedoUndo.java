package eg.edu.alexu.csd.oop.draw;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;

public class RedoUndo implements Observer{

	private Stack<List<Shape>> historyUndo;
	private Stack<List<Shape>> historyRedo ;


	public RedoUndo() {
		historyUndo = new Stack<List<Shape>>();
		historyUndo.add(new ArrayList<Shape>());
		historyRedo = new Stack<List<Shape>>();
	}
	
	public List<Shape> undo(){
		try{

			historyRedo.add(new ArrayList<Shape>(historyUndo.pop()));
			return new ArrayList<Shape>(historyUndo.peek());

		}catch(EmptyStackException ex){

			historyUndo.add(new ArrayList<Shape>(historyRedo.pop()));
			throw new RuntimeException("Out of history bounds");
		}
	}
	
	public List<Shape> redo(){
		try{
			List<Shape> shape = new ArrayList<Shape>(historyRedo.pop());
			historyUndo.add(new ArrayList<Shape>(shape));
			return shape;
		}catch(EmptyStackException ex){
			throw new RuntimeException("Out of history bounds");
		}
	}
	
	public void clear(){
		historyRedo.clear();
		historyUndo.clear();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void update(Observable arg0, Object shape) {

		if(historyUndo.size() < 20){
			historyUndo.add(new ArrayList<Shape>((List<Shape>)shape));
		}
		else{
			historyUndo.remove(0);
			historyUndo.add(new ArrayList<Shape>((List<Shape>)shape));
		}
		historyRedo.clear();
	}

}
