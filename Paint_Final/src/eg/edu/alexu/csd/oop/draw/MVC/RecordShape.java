package eg.edu.alexu.csd.oop.draw.MVC;

import java.awt.Color;
import java.awt.Point;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.JOptionPane;

import eg.edu.alexu.csd.oop.draw.Factory;
import eg.edu.alexu.csd.oop.draw.Shape;

// implemented only by the controller

public class RecordShape{
	private Shape shape;
	private Set<String> s;
	private String currentShape;
	private Point begin, end;
	private Color stroke = Color.BLACK;
	private Color fill = Color.RED;
	private Map<String, Double> m;	
	
	public RecordShape(String currentShape, Point begin, Point end,
			Color stroke, Color fill) {
		this.currentShape = currentShape;
		this.begin = begin;
		this.end = end;
		this.stroke = stroke;
		this.fill = fill;
	}
	
	
	
	public void fillMapProperties(){

		Iterator<String> it = s.iterator();
		while(it.hasNext()){

			final String str1 = it.next();

			if(str1.equals("Width") || str1.equals("Major Axe") || str1.equals("Length") || str1.equals("Diameter")){
				m.put(str1, Double.parseDouble((""+(Math.abs( begin.x - end.x )))));
			}else if(str1.equals("Height") || str1.equals("Minor Axe") || str1.equals("Side of Triangle")){
				m.put(str1, Double.parseDouble((""+(Math.abs(begin.y - end.y )))));
			}else if(str1.equals("X-axis"))
			{
				m.put(str1 , Double.parseDouble((""+end.x)));
				System.out.println(str1 + "    " + end.x);
			}else if(str1.equals("Y-axis"))
			{
				m.put(str1 , Double.parseDouble((""+end.y)));
				System.out.println(str1 + "    " + end.y);
			}
			else{
				boolean flag = false;
				while(!flag){
					try{
						m.put(str1, Double.parseDouble(JOptionPane.showInputDialog(null, "Enter "+str1 )));
						flag = true;
					}catch(NumberFormatException ex){
						View.displayErrorMessage("Wrong entry");
					}
				}			}
		}
	}

	public Shape setShape(){
		shape.setProperties(new HashMap<>(m));
		shape.setColor(stroke);
		shape.setFillColor(fill);
		m = new LinkedHashMap<String , Double>();
		
		return shape;
	}
	//Take position from mouse
	private Point takePosition(){
		Point p = new Point(Math.min(begin.x, end.x) , Math.min(begin.y, end.y));
		if(currentShape.equals("eg.edu.alexu.csd.oop.draw.shapes.Line")){
			p.x = begin.x ; p.y = begin.y;
		}
		return p;
	}

	// setting the properties map according to each shape object
	public void setKeysAndPos(){
		// The map of properties here contains only the keys with no values	
		try {
			shape = Factory.getInstance().getShape(currentShape);
			m = shape.getProperties();
			shape.setPosition(takePosition());
			if(m != null)
				s = m.keySet();
			else
				s = new HashSet<>();
		} catch (ClassNotFoundException | NoSuchMethodException
				| InstantiationException | IllegalAccessException
				| InvocationTargetException e) {
			View.displayErrorMessage("Error loading class");
			e.printStackTrace();
		}
	}
}
