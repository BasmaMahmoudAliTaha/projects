package eg.edu.alexu.csd.oop.draw.shapes;

import java.awt.Graphics;
import java.awt.Point;
import java.util.LinkedHashMap;
import java.util.Map;

import eg.edu.alexu.csd.oop.draw.ShapeDrawer;

public class Circle extends ShapeDrawer{

	public Circle() {
		Map<String , Double> m = new LinkedHashMap<String , Double>();
		m.put("Diameter", 0.0);
		setProperties(m);	
	}

	@Override
	public void draw(Graphics canvas) {
		Map<String , Double> m = getProperties();
		Point p = this.getPosition();
		canvas.setColor(this.getColor());
		canvas.drawOval((int)p.getX() , (int) p.getY() , ((int)m.get("Diameter").doubleValue()),((int) m.get("Diameter").doubleValue()));
		canvas.setColor(this.getFillColor());
		canvas.fillOval( (int) p.getX() , (int) p.getY(), ((int)m.get("Diameter").doubleValue()),((int) m.get("Diameter").doubleValue()));
	}
}
