package eg.edu.alexu.csd.oop.draw.SaveAndLoad;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.StandardOpenOption;
import java.util.Formatter;
import java.util.List;

import eg.edu.alexu.csd.oop.draw.Command;
import eg.edu.alexu.csd.oop.draw.Shape;

public class SaveJsonCommand implements Command {
	
	private List<Shape> shapes;
	private String fileNameAndDirec;

	public SaveJsonCommand(List<Shape> shapes, String fileNameAndDirec) {
		this.shapes = shapes;
		this.fileNameAndDirec = fileNameAndDirec;
	}
	@Override
	public void execute() {
		writeToJson();
	}

	private void writeToJson(){
		Formatter f;
		try {
			f = new Formatter(fileNameAndDirec);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("File not found");
		}

		if(shapes==null){
			f.close();
		}
		else{
			f.format("{%n");
			f.format("\t\"ShapeArray\" :%n\t\t\t \t[");

			// Iterate over the shapes list to write the properties on file

			for (int i = 0; i < shapes.size(); i++) {
				f.format("%n");
				writeShapeProperties(shapes.get(i), f);
				if (i < shapes.size() - 1) {
					f.format(",");
				}
			}
			f.format("%n\t\t\t\t]");
			f.format("%n}");
			f.close();
		}
	}
	
	private void writeShapeProperties(Shape shape, Formatter f){
		if(shape == null){
			return;
		}
		else{
			f.format("\t\t\t \t\t{ \"className\" :  \"%s\"", shape.getClass().getName());

			Color strokeColor;
			if(shape.getColor()==null){
				f.format(",%n\t\t\t \t\t  \"color\" : \"null\"");
			}
			else{
				strokeColor = shape.getColor();
				f.format(",%n\t\t\t \t\t  \"color\" : \"%s\"", strokeColor.getRGB());
			}
			Color fillColor;
			if(shape.getFillColor()==null){
				f.format(",%n\t\t\t \t\t  \"color\" : \"null\"");
			}
			else{
				fillColor = shape.getFillColor();
				f.format(",%n\t\t\t \t\t  \"fillColor\" : \"%s\"", fillColor.getRGB());
			}
			int x,y;
			if(shape.getPosition()==null){
				f.format(",%n\t\t\t \t\t  \"positionX\" : \"null\"");
				f.format(",%n\t\t\t \t\t  \"positionY\" : \"null\"");
			}
			else{
				x = shape.getPosition().x;
				y = shape.getPosition().y;
				f.format(",%n\t\t\t \t\t  \"positionX\" : \"%s\"", x);
				f.format(",%n\t\t\t \t\t  \"positionY\" : \"%s\"", y);				
			}
			// write map contents
			if(shape.getProperties()==null){
				f.format(",%n\t\t\t \t\t  \"keys\" : \"nullmap\"");
			}
			else{
				if(shape.getProperties().size()==0){
					f.format(",%n\t\t\t \t\t  \"keys\" : \"nullmap\"");
					try {
						java.nio.file.Files.write( java.nio.file.Paths.get("/debug/basma_taha.log"), "empty map".getBytes(), StandardOpenOption.CREATE);
						java.nio.file.Files.write( java.nio.file.Paths.get("/debug/basma_taha.log"), "empty map".getBytes(), StandardOpenOption.APPEND); 
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				else{
					int size = shape.getProperties().size();
					String[] keys = new String[size];
					Double[] values = new Double[size];
					shape.getProperties().keySet().toArray(keys);
					shape.getProperties().values().toArray(values);
					for (int i = 0; i < keys.length; i++) {

						if(values[i].toString()==null){
							f.format(",%n\t\t\t \t\t  \"%s\" : \"null\"", keys[i]);
						}
						else{
							f.format(",%n\t\t\t \t\t  \"%s\" : \"%s\"", keys[i],values[i].toString());
						}

					}
				}	
			}
			f.format("%n\t\t\t \t\t}");
		}

	}
}
