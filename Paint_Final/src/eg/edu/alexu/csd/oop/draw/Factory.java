package eg.edu.alexu.csd.oop.draw;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Factory {
	
	private static Factory instance;
	
	public static Factory getInstance(){
		if(instance == null)
			instance = new Factory();
		return instance;
	}
	
	private Factory(){}
	
	public Shape getShape(String s) throws ClassNotFoundException , NoSuchMethodException , InstantiationException, IllegalAccessException,InvocationTargetException{
		Class<?> cls = Class.forName(s);
		Constructor<?> constructor = cls.getConstructor();
		Shape shape = (Shape) constructor.newInstance();
		return shape;
	}
}
