package eg.edu.alexu.csd.oop.draw.shapes;

import java.awt.Graphics;
import java.awt.Point;
import java.util.LinkedHashMap;
import java.util.Map;

import eg.edu.alexu.csd.oop.draw.ShapeDrawer;

public class Rectangle extends ShapeDrawer{

	public Rectangle() {
		Map<String , Double> m = new LinkedHashMap<String , Double>();
		m.put("Width", 0.0);
		m.put("Height", 0.0);
		setProperties(m);
	}

	@Override
	public void draw(Graphics canvas) {
		Map<String , Double> m = getProperties();
		Point p = this.getPosition();
		
		canvas.setColor(this.getColor());
		canvas.drawRect((int) p.getX() , (int) p.getY(), (int)m.get("Width").doubleValue(),(int) m.get("Height").doubleValue());
		canvas.setColor(this.getFillColor());
		canvas.fillRect((int) p.getX() , (int) p.getY(), (int)m.get("Width").doubleValue(),(int) m.get("Height").doubleValue());

	}

}
