package eg.edu.alexu.csd.oop.draw.MVC;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.Box;
import javax.swing.JOptionPane;

import eg.edu.alexu.csd.oop.draw.Shape;

public class View  {

	private JFrame frame;
	private Image img1, img2, img3, img4, img5, img6 , img7 , img8 , img9 , img10 , img11 , img12 , img13,img14, img15, img16, img17 ;
	private Canvas canvas;
	private JButton resizeB;
	private JButton moveB;
	private JButton changeColor;
	private JButton deleteB;
	private List<JButton> shapesButton = new ArrayList<JButton>();
	private List<Class<? extends Shape>> supportedShapes ; 
	private JButton  undoB, redoB,strokeColB ,fillColB ,saveB ,loadB ;
	private Box myBox = Box.createVerticalBox();
	private static View firstInstance = null;
	/**
	 * Create the application.
	 */
	private View() {
	}
	
	public static View getInstance(){
		if(firstInstance == null){
			firstInstance = new View();
		}
		return firstInstance;
	}


	
	public Canvas getCanvas(){
		return canvas;
	}
	public void setSupportedShapes(List<Class<? extends Shape>> supportedShapes){
		this.supportedShapes = supportedShapes;
	}
	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		List<Image> imageList = new ArrayList<Image>();
		frame = new JFrame();
		frame.getContentPane().setBackground(UIManager.getColor("Button.select"));
		frame.setBackground(UIManager.getColor("Button.shadow"));
		frame.setBounds(100, 100, 700, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.setLocationRelativeTo(null);

		img1 = new ImageIcon(this.getClass().getResource("/circ.png")).getImage(); imageList.add(img1);
		img2 = new ImageIcon(this.getClass().getResource("/square.png")).getImage(); imageList.add(img2);
		img3 = new ImageIcon(this.getClass().getResource("/rec.png")).getImage(); imageList.add(img3);
		img4 = new ImageIcon(this.getClass().getResource("/tri.png")).getImage(); imageList.add(img4);
		img5 = new ImageIcon(this.getClass().getResource("/line.png")).getImage(); imageList.add(img5);
		img6 = new ImageIcon(this.getClass().getResource("/ellipse.png")).getImage(); imageList.add(img6);
		img7 = new ImageIcon(this.getClass().getResource("/Stroke.png")).getImage();
		img8 = new ImageIcon(this.getClass().getResource("/Brush.png")).getImage();
		img9 = new ImageIcon(this.getClass().getResource("/delete.png")).getImage();
		img10 = new ImageIcon(this.getClass().getResource("/move.png")).getImage();
		img11 = new ImageIcon(this.getClass().getResource("/resize.png")).getImage();
		img12 = new ImageIcon(this.getClass().getResource("/save.png")).getImage();
		img13 = new ImageIcon(this.getClass().getResource("/changeColor.png")).getImage();
		img14 = new ImageIcon(this.getClass().getResource("/load.png")).getImage();
		img15 = new ImageIcon(this.getClass().getResource("/undo.png")).getImage();
		img16 = new ImageIcon(this.getClass().getResource("/redo.png")).getImage();
		img17 = new ImageIcon(this.getClass().getResource("/def.png")).getImage();
		
		
		canvas = new MyCanvas();
		canvas.setBackground(Color.WHITE);
		frame.getContentPane().add(canvas,BorderLayout.CENTER);

		JScrollPane scrollPane = new JScrollPane();

		int counter = 0;
		for(Class<? extends Shape> supportClass : supportedShapes){
			JButton button = createButton(supportClass);
			if(counter < imageList.size())
				button.setIcon(new ImageIcon(imageList.get(counter)));
			counter++;
		}
		
		strokeColB = new JButton();
		strokeColB.setToolTipText("Set stroke color");
		strokeColB.setBackground(new Color(255, 255, 255));
		strokeColB.setIcon(new ImageIcon(img7));
		myBox.add(strokeColB);

		fillColB = new JButton();
		fillColB.setToolTipText("Set fill color");
		fillColB.setBackground(new Color(255, 255, 255));
		fillColB.setIcon(new ImageIcon(img8));
		
		myBox.add(fillColB);

		deleteB = new JButton();
		deleteB.setToolTipText("Delete shape");
		deleteB.setBackground(new Color(255, 255, 255));
		deleteB.setIcon(new ImageIcon(img9));
		deleteB.setActionCommand("delete");
		myBox.add(deleteB);
		undoB = new JButton();
		undoB.setToolTipText("Undo");
		undoB.setBackground(new Color(255, 255, 255));
		undoB.setIcon(new ImageIcon(img15));
		undoB.setActionCommand("undo");
		myBox.add(undoB);

	
		redoB = new JButton();
		redoB.setToolTipText("Redo");
		redoB.setBackground(new Color(255, 255, 255));
		redoB.setIcon(new ImageIcon(img16));
		redoB.setActionCommand("redo");
		myBox.add(redoB);

		changeColor = new JButton();
		changeColor.setToolTipText("Change color of the drawing shape");
		changeColor.setBackground(new Color(255, 255, 255));
		changeColor.setIcon(new ImageIcon(img13));
		changeColor.setActionCommand("changeColor");
		myBox.add(changeColor);

		saveB = new JButton();
		saveB.setToolTipText("Save file");
		saveB.setBackground(new Color(255, 255, 255));
		saveB.setIcon(new ImageIcon(img12));
		saveB.setActionCommand("save");
		myBox.add(saveB);
		
		loadB = new JButton();
		loadB.setToolTipText("Load file");
		loadB.setBackground(new Color(255, 255, 255));
		loadB.setIcon(new ImageIcon(img14));
		loadB.setActionCommand("load");
		myBox.add(loadB);
		
		moveB = new JButton();
		moveB.setToolTipText("Move shape according to upper left corner");
		moveB.setBackground(new Color(255, 255, 255));
		moveB.setIcon(new ImageIcon(img10));
		moveB.setActionCommand("move");
		myBox.add(moveB);

		resizeB = new JButton();
		resizeB.setToolTipText("Resize shape according to upper left corner");
		resizeB.setBackground(new Color(255, 255, 255));
		resizeB.setIcon(new ImageIcon(img11));
		resizeB.setActionCommand("resize");
		myBox.add(resizeB);

		scrollPane.setViewportView(myBox);
		frame.add(scrollPane , BorderLayout.WEST);		
		frame.setVisible(true);
	}
	
	private JButton createButton(Class<? extends Shape> name ){
		JButton button = new JButton();
		button.setToolTipText("Draw " + name.getSimpleName());
		button.setBackground(new Color(255, 255, 255));
		button.setIcon(new ImageIcon(img17));
		System.out.println(name.getName());
		button.setActionCommand(name.getName());
		myBox.add(button);
		SwingUtilities.updateComponentTreeUI(frame);
		shapesButton.add(button);
		return button;
	}

	
	public static void displayErrorMessage(String errMessage){
		JOptionPane.showMessageDialog(null, errMessage);
	}
	public void addMouseMotionListener(MouseAdapter mouse){
		canvas.addMouseListener(mouse);
	}



	private class MyCanvas extends Canvas{
		private static final long serialVersionUID = 1234567L;
		@Override 
		public Graphics getGraphics(){
			Graphics g = super.getGraphics();
			Graphics2D g2 = (Graphics2D)g;
			g2.setStroke(new BasicStroke(8.0f));
			return g2;
		}
		
		@Override
		public void paint(Graphics g){
			super.paint(g);
		}
	}
	// listener for the shapes' buttons, move, delete, resize and change colors
	public void addBtnNameListener(ActionListener ListenForCurrentShapeName){
		for(JButton button : shapesButton)
			button.addActionListener(ListenForCurrentShapeName);
		deleteB.addActionListener(ListenForCurrentShapeName);
		moveB.addActionListener(ListenForCurrentShapeName);
		resizeB.addActionListener(ListenForCurrentShapeName);
		changeColor.addActionListener(ListenForCurrentShapeName);
	}
	/* listeners for save btn,
	 * Load btn
	 * undo btn
	 * re-do btn
	 * stroke color btn
	 * fill color btn
	 */
	public void addSaveBtnListener(ActionListener ListenForSave){
		saveB.addActionListener(ListenForSave);
	}

	public void addLoadBtnListener(ActionListener ListenForLoad){
		loadB.addActionListener(ListenForLoad);
	}

	public void addUndoBtnListener(ActionListener ListenForUndo){
		undoB.addActionListener(ListenForUndo);
	}

	public void addRedoBtnListener(ActionListener ListenForRedo){
		redoB.addActionListener(ListenForRedo);
	}

	public void addStrokeColBtnListener(ActionListener ListenForStrokeCol){
		strokeColB.addActionListener(ListenForStrokeCol);
	}
	
	public void addFillColBtnListener(ActionListener ListenForFillCol){
		fillColB.addActionListener(ListenForFillCol);
	}
}