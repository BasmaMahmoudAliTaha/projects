package eg.edu.alexu.csd.oop.draw;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.StringTokenizer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class Plugin {

	@SuppressWarnings("unchecked")
	public static List<Class<? extends Shape>> addPlugin() throws ClassNotFoundException,IllegalAccessException,InstantiationException, IOException{
		List<Class<? extends Shape>> classes = new ArrayList<Class<? extends Shape>>();
		String path = System.getProperty("java.class.path");
		ClassLoader cl = Plugin.class.getClassLoader();
		System.out.println(path);
		StringTokenizer st = new StringTokenizer(path , System.getProperty("path.separator"));
		List<File> matches = new ArrayList<File>();
		while(st.hasMoreElements()){
			String jarPath = (String) st.nextElement() ;
			File file = new File(jarPath);
			if(file.isFile()){
				if(jarPath.endsWith(".jar") && !jarPath.endsWith("vector.jar")){
					System.out.println("JAR"+jarPath);
					matches.add(file);
				}
			}else{
				File[] matche = file.listFiles(new FilenameFilter()
				{
					public boolean accept(File dir, String name)
					{
						return  name.endsWith(".jar");
					}
				});

				for(File f : matche)
					matches.add(f);
			}
			if(matches.size() == 0 )
				continue;

			for(File f : matches){
				JarFile jarfile = new JarFile(f); 
				Enumeration<JarEntry> enu = jarfile.entries();
				while(enu.hasMoreElements())
				{
					JarEntry je = enu.nextElement();
					System.out.println("JAR ENTRY : "+je.getName());
					if(je.isDirectory() || !je.getName().endsWith(".class"))
						continue;
					if( je.getName().endsWith(".class")){
						String s = je.getName();
						s = s.split( "\\.")[0];
						s = s.replaceAll("/", "\\.");
						System.out.println(s);
						Class<?> myObjectClass =  cl.loadClass(s); 
						if(Shape.class.isAssignableFrom(myObjectClass)){
							classes.add( (Class<? extends Shape>) myObjectClass);
						}
					}
				}
				jarfile.close();
			}

		}

		return classes;

	}

}
