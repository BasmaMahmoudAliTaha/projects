package eg.edu.alexu.csd.oop.draw.shapes;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.util.LinkedHashMap;
import java.util.Map;

import eg.edu.alexu.csd.oop.draw.ShapeDrawer;

public class Triangle extends ShapeDrawer {

	public Triangle(){
		Map<String , Double> m = new LinkedHashMap<String , Double>();
		m.put("Side of Triangle", 0.0);
		setProperties(m);
	}
	
	
	@Override
	public void draw(Graphics canvas) {
		Map<String , Double> m = getProperties();
		Point p = this.getPosition();
		
		canvas.setColor(this.getColor());
		canvas.drawPolygon(new Polygon( new int[]{p.x, p.x , p.x + (int)m.get("Side of Triangle").doubleValue()  }, new int[]{p.y, p.y + (int)m.get("Side of Triangle").doubleValue() , p.y} , 3));
		canvas.setColor(this.getFillColor());
		canvas.fillPolygon(new Polygon( new int[]{p.x, p.x , p.x + (int)m.get("Side of Triangle").doubleValue()  }, new int[]{p.y, p.y + (int)m.get("Side of Triangle").doubleValue() , p.y} , 3));
		
	}

}
