package eg.edu.alexu.csd.oop.draw.SaveAndLoad;

import java.awt.Color;
import java.io.File;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import eg.edu.alexu.csd.oop.draw.Command;
import eg.edu.alexu.csd.oop.draw.Shape;

public class SaveXmlCommand implements Command {
	private List<Shape> shapes;
	private String fileNameAndDirec;

	public SaveXmlCommand(List<Shape> shapes, String fileNameAndDirec) {
		this.shapes = shapes;
		this.fileNameAndDirec = fileNameAndDirec;
	}
	@Override
	public void execute() {
		WritoToXML();
	}
	private void WritoToXML(){
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = null;
			if(shapes==null||shapes.isEmpty()){
				doc = docBuilder.newDocument();
				Element Shape = doc.createElement("Shape");
				Shape.appendChild(doc.createTextNode("null"));
				doc.appendChild(Shape);
			}
			else{
				doc = docBuilder.newDocument();
				Element Shape = doc.createElement("Shape");
				doc.appendChild(Shape);

				for(Shape s : shapes){

					if(!(s==null)){
						String  className = s.getClass().getName().toString();
						Element shapeType = doc.createElement(className);
						Shape.appendChild(shapeType);
						// properties elements

						Map<String, Double> m  ;
						if(s.getProperties() == null||s.getProperties().isEmpty()){
							Element nullMap = doc.createElement("nullmap");
							nullMap.appendChild(doc.createTextNode("null"));
							shapeType.appendChild(nullMap);
						}
						else{
							m = s.getProperties();
							for(Map.Entry<String, Double> entry : m.entrySet())
							{
								System.out.println((String)entry.getKey().replaceAll("\\s+","")+" "+entry.getValue());

								Element key = doc.createElement((String)entry.getKey().replaceAll("\\s+",""));
								String str = String.valueOf((Double)entry.getValue());
								if(str!=null)
									key.appendChild(doc.createTextNode(str));
								else{
									key.appendChild(doc.createTextNode("null"));
								}
								shapeType.appendChild(key);
							}
						}
						Element color = doc.createElement("color");
						Color strokeCol  ;
						if(s.getColor()==null){
							color.appendChild(doc.createTextNode("null"));
							shapeType.appendChild(color);
						}
						else{
							strokeCol = s.getColor();
							String colorCode = Integer.toString(strokeCol.getRGB());
							color.appendChild(doc.createTextNode(colorCode));
							shapeType.appendChild(color);
						}

						Element fillColor = doc.createElement("fillColor");
						Color fillco ;
						if(s.getFillColor()==null){
							fillColor.appendChild(doc.createTextNode("null"));
							shapeType.appendChild(fillColor);
						}
						else{
							fillco = s.getFillColor();
							String colorCode = Integer.toString(fillco.getRGB());
							fillColor.appendChild(doc.createTextNode(colorCode));
							shapeType.appendChild(fillColor);
						}
						Element positionX = doc.createElement("positionX");
						Element positionY = doc.createElement("positionY");
						if(s.getPosition()==null){
							positionX.appendChild(doc.createTextNode("null"));
							shapeType.appendChild(positionX);
							positionY.appendChild(doc.createTextNode("null"));
							shapeType.appendChild(positionY);
						}
						else{
							positionX.appendChild(doc.createTextNode(String.valueOf(s.getPosition().getX())));
							shapeType.appendChild(positionX);
							positionY.appendChild(doc.createTextNode(String.valueOf(s.getPosition().getY())));
							shapeType.appendChild(positionY);
						}
					}			
				}
			}



			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			File f = new File(fileNameAndDirec);

			StreamResult result = new StreamResult(f);

			transformer.transform(source, result);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}

}
