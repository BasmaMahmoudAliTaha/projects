package eg.edu.alexu.csd.oop.draw.SaveAndLoad;

import java.awt.Color;
import java.awt.Point;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eg.edu.alexu.csd.oop.draw.Command;
import eg.edu.alexu.csd.oop.draw.Factory;
import eg.edu.alexu.csd.oop.draw.Shape;

public class LoadXmlCommand implements Command {

	private String fileNameAndDirec;
	private  List<Shape>  loadedList;

	public LoadXmlCommand (String fileNameAndDirec) {
		this.fileNameAndDirec = fileNameAndDirec;
	}

	
	@Override
	public void execute() {
		readFromXML();
	}

	public List<Shape> getLoadedList(){
		return loadedList;
	}

	private void readFromXML(){

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(); 
		Document dom;
		loadedList = new ArrayList<Shape>(); 
		try {

			//Using factory get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			//parse using builder to get DOM representation of the XML file
			dom = db.parse(fileNameAndDirec);
			// normalize the tree text nodes to make all of them in the same full depth 

			dom.getDocumentElement().normalize();
			// get document root element
			Element docEle = dom.getDocumentElement();

			// if no shapes or shapes is set to null, do nothing 
			// else do the following
			if(!docEle.getTextContent().equals("null")){
				//get a nodelist of elements
				NodeList nl = docEle.getChildNodes();
				if(nl != null && nl.getLength() > 0) {
					for(int i = 0 ; i < nl.getLength();i++) {
						Element el = (Element) nl.item(i);
						// get the class name of the shape 
						String str1 = el.getNodeName();
						Shape shape;
						try {
							shape = Factory.getInstance().getShape(str1);
							Map<String, Double> m = shape.getProperties();

							if (nl.item(i).getNodeType() == Node.ELEMENT_NODE) {
								NodeList properties = nl.item(i).getChildNodes();
								int j = 0;
								if(properties.item(j).getNodeName().equals("nullmap")){
									//do nothing and let the m of the shape to its default
									j++; // go to next line
								}
								else{
									Set<String> keys = m.keySet();
									for(String k : keys){
										String str ;
										if(!properties.item(j).getTextContent().equals("null")){
											str = properties.item(j).getTextContent();
											Double val = Double.parseDouble(str);
											m.put(k,val);
										}
										j++;
									}
									shape.setProperties(m);
								}

								String strokeCol;
								if(!properties.item(j).getTextContent().equals("null")){
									strokeCol = properties.item(j).getTextContent();
									shape.setColor(new Color(Integer.parseInt(strokeCol)));
								}
								j++;

								String fillCol;

								if(!properties.item(j).getTextContent().equals("null")){
									fillCol = properties.item(j).getTextContent();
									shape.setFillColor(new Color(Integer.parseInt(fillCol)));
								}
								j++;

								double y = -1.0,x = -1.0;
								String xPos ;

								if(!properties.item(j).getTextContent().equals("null")){
									xPos = properties.item(j).getTextContent();
									x  = Double.parseDouble(xPos);
								}

								j++;

								String yPos ;
								if(!properties.item(j).getTextContent().equals("null")){
									yPos = properties.item(j).getTextContent();
									y = Double.parseDouble(yPos);
								}

								if(x!=-1.0 && y!=-1.0){
									Point p = new Point();
									p.x = (int) x;
									p.y = (int) y;
									shape.setPosition(p);
								}
								loadedList.add(shape);
							}
						} catch (ClassNotFoundException | NoSuchMethodException
								| InstantiationException | IllegalAccessException
								| InvocationTargetException e) {

							e.printStackTrace();
						}

					}
				}
			}
		}
		catch(ParserConfigurationException |SAXException |IOException  e) {
			throw new RuntimeException("File not found!");
		}
	}
}
