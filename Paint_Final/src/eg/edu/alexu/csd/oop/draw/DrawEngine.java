package eg.edu.alexu.csd.oop.draw;

import java.awt.Graphics;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.RuntimeErrorException;

import eg.edu.alexu.csd.oop.draw.Plugin;
import eg.edu.alexu.csd.oop.draw.SaveAndLoad.*;
import eg.edu.alexu.csd.oop.draw.shapes.*;


public class DrawEngine  extends Observable implements DrawingEngine {

	private List<Shape> shape; 
	private RedoUndo redoUndo ;

	public DrawEngine() {
		shape = new ArrayList<Shape>(); 
		redoUndo = new RedoUndo();
		this.addObserver(redoUndo);
	}
	
	@Override
	public void refresh(Graphics canvas) {
		for(Shape s : this.shape){
			s.draw(canvas);
		}
	}

	@Override
	public void addShape(Shape shape) {
		if(shape != null){
			this.shape.add(shape);
			this.setChanged();
			this.notifyObservers(this.shape);
		}
		else 
			throw new RuntimeErrorException(null);
	}

	@Override
	public void removeShape(Shape shape) {
		if(shape != null && this.shape.contains(shape)){
			this.shape.remove(shape);
			this.setChanged();
			this.notifyObservers(this.shape);
		}

		else
			throw new RuntimeErrorException(null);
	}

	@Override
	public void updateShape(Shape oldShape, Shape newShape) {
		if(this.shape.contains(oldShape)){
			try {
				Shape s = (Shape)oldShape.clone();
				s.setProperties(newShape.getProperties());
				s.setColor(newShape.getColor());
				s.setPosition(newShape.getPosition());
				s.setFillColor(newShape.getFillColor());
				this.shape.remove(oldShape);
				this.shape.add(s);
				this.setChanged();
				this.notifyObservers(this.shape);
			} catch (CloneNotSupportedException | NullPointerException e) {
				throw new RuntimeErrorException(null);
			}
		}else 
			throw new RuntimeErrorException(null);
	}

	@Override
	public Shape[] getShapes() {
		if(this.shape.size() != 0)
			return shape.toArray(new Shape[0]);
		else
			return new Shape[]{};
	}

	
	@Override
	public List<Class<? extends Shape>> getSupportedShapes() {
		try{
			List<Class<? extends Shape>> supportedShapes = new ArrayList<Class<? extends Shape>>();
			supportedShapes.add(Circle.class);
			supportedShapes.add(Square.class);
			supportedShapes.add(Rectangle.class);
			supportedShapes.add(Triangle.class);
			supportedShapes.add(Line.class);
			supportedShapes.add(Ellipse.class);
			List<Class<? extends Shape>> list = Plugin.addPlugin();
			for(Class<? extends Shape> c : list)
				supportedShapes.add(c);
			return supportedShapes;
		}catch(ClassNotFoundException | IllegalAccessException | InstantiationException | IOException ex){
			ex.printStackTrace();
			throw new RuntimeErrorException(null);
		}
	}

	@Override
	public void undo() {
		this.shape = redoUndo.undo();
	}

	@Override
	public void redo() {
		this.shape = redoUndo.redo();
	}

	@Override
	public void save(String path) {
		String original = path;
		path = path.toLowerCase();
		Pattern p = Pattern.compile("\\.(xml|json)");
		Matcher m = p.matcher(path); 
		
		if(m.find()){
			ExecuteSaveAndLoadCommand onPress ;
			if(m.group(1).equals("xml")){
				
				SaveXmlCommand saveXml = new SaveXmlCommand(shape, original);
				onPress = new ExecuteSaveAndLoadCommand(saveXml);
				try{
					onPress.press();
				}catch(RuntimeException e){
					throw new RuntimeException(e.getMessage());
				}
			}
			
			else if(m.group(1).equals("json")){
				SaveJsonCommand saveJson = new SaveJsonCommand(shape, original);
				onPress = new ExecuteSaveAndLoadCommand(saveJson);
				try{
					onPress.press();
				}catch(RuntimeException e){
					throw new RuntimeException(e.getMessage());
				}
			}
		}
		else{
			throw new RuntimeException("File extenstion error!");
		}
	}

	@Override
	public void load(String path) {
		String original = path;
		path = path.toLowerCase();
		shape.clear();
		
		Pattern p = Pattern.compile("\\.(xml|json)");
		Matcher m = p.matcher(path); 
		
		if(m.find()){
			ExecuteSaveAndLoadCommand onPress  = null;
			
			if(m.group(1).equals("xml")){
				LoadXmlCommand loadXml = new LoadXmlCommand(original);
				onPress = new ExecuteSaveAndLoadCommand(loadXml);
			}
			
			else if(m.group(1).equals("json")){
				
				LoadJsonCommand loadJson = new LoadJsonCommand(original);
				onPress = new ExecuteSaveAndLoadCommand(loadJson);
			}
			try{
				onPress.press();
			}
			catch(RuntimeException e){
				throw new RuntimeException(e.getMessage());
			}
			shape = onPress.getLoadedList();
			redoUndo.clear();
			
			if(shape!=null){
				this.setChanged();
				this.notifyObservers(shape);
			}
		}
		else{
			throw new RuntimeException("File extenstion error!");
		}

	}

}